module VSet = Set.Make(String)


let translate_program e =
  let fdefs = ref [] in
  let new_fname =
    let cpt = ref (-1) in
    fun () -> incr cpt; Printf.sprintf "fun_%i" !cpt
  in
  
  
  let rec tr_expr (e: Fun.expression) (bvars: VSet.t):
      Clj.expression * (string * int) list =
    let cvars = ref [] in
    let new_cvar =
      let cpt = ref 0 in (* commencera à 1 *)
      fun x -> incr cpt; cvars := (x, !cpt) :: !cvars; !cpt
    in
    
    let rec convert_var x bvars =
      Clj.(if VSet.mem x bvars
        then Name(x)
        else if List.mem_assoc x !cvars
        then CVar(List.assoc x !cvars)
        else CVar(new_cvar x))
    and crawl_list el bvars = match el with
      | [] ->  []     
      | [x] ->  [crawl x bvars]
      | x::l -> let ret = crawl x bvars in
                  let l2 = crawl_list l bvars in
                    ret::l2
    and crawl e bvars = match e with
      | Fun.Cst(n) ->
        Clj.Cst(n)

      | Fun.Bool(b) ->
        Clj.Bool(b)
          
      | Fun.Var(x) ->
        Clj.Var(convert_var x bvars)
        
      

      | Fun.Binop(op, e1, e2) ->
        Clj.Binop(op, crawl e1 bvars, crawl e2 bvars)


      | Fun.LetIn(x, e1, e2) ->
        Clj.LetIn(x, crawl e1 bvars, crawl e2 (VSet.add x bvars))
      
      
      | Fun.If (e1,e2,e3) ->
        Clj.If (crawl e1 bvars ,crawl e2 bvars ,crawl e3 bvars)  (* to be checked*)
      
      | Fun.LetRec(x, e1, e2) ->
        Clj.LetRec(x, crawl e1 bvars, crawl e2 (VSet.add x bvars))

      | Fun.Tpl(e) ->
      Clj.Tpl(  crawl_list e bvars  )
      
        
      | Fun.TplGet(e,i)->
        Clj.TplGet(crawl e bvars ,i)
      
      | Fun.App(e1,e2)->
        Clj.App(crawl e1 bvars, crawl e2 bvars)
                      
      | Fun.Fun(s,e) ->
        let name = new_fname() in 
        let param = s in
        let code = crawl e (VSet.add s VSet.empty) in
        fdefs := !fdefs @ [{Clj.name; Clj.code; Clj.param}];
        
        Clj.Tpl( [Clj.FunRef(name); code ]    )

    in
    let te = crawl e bvars in
    te, !cvars

  in
  let code, _ = tr_expr e VSet.empty in
  Clj.({
    functions = !fdefs;
    code = code;
  })
