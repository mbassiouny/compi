open Types
open Ops

module Smap = Map.Make(String)
type tenv = ty Smap.t



let type_program prog =
      
  let rec type_expr e env =
    let e, t = compute_type e env in
    { Tml.e = e; Tml.t = t }
    
  and type_expr_list  el env  = match el with
    | [] ->  []     
    | [x] ->  (match x with 
                | (s,ex) ->    [(s, type_expr ex env)]   )
    | x::l -> (match x with 
              | (s,ex) ->  
              let ret =   (s, type_expr ex env)   in
              let l2 = type_expr_list l env in
              ret::l2       )
  and compute_type e env = match e with
    | Mml.Cst n -> Tml.Cst n, TInt

    | Mml.Bool b -> Tml.Bool b, TBool
      
    | Mml.Var x -> Tml.Var x, Smap.find x env
      
    | Mml.Binop(op, e1, e2) ->
      let te1 = type_expr e1 env in
      let te2 = type_expr e2 env in
      let tops, tr = match op with
        | Add | Sub | Mul | Div | Rem | Lsl | Lsr -> TInt, TInt
      in
      check te1.t tops;
      check te2.t tops;
      Tml.Binop(op, te1, te2), tr
        
    | Mml.LetIn(x, e1, e2) ->
      let te1 = type_expr e1 env in
      let te2 = type_expr e2 (Smap.add x te1.t env) in
      Tml.LetIn(x, te1, te2), te2.t

    | Mml.If(e0, e1, e2) ->
      let te0 = type_expr e0 env in
      (* check bool *)
      check te0.t TBool;
      let te1 = type_expr e1 env in
      let te2 = type_expr e2 env in
      (* check type e1 = type e2*)
      check te1.t te2.t;
      
      Tml.If(te0, te1, te2), te2.t

      

      | Mml.App(e0, e1)->  
        let te0 = type_expr e0 env in
        let te1 = type_expr e1 env in
        Tml.App(te0, te1), te1.t
    | Mml.Fun(s, t, e)->  (*check type e = t  *)
      let te = type_expr e env in
      check te.t t;
      Tml.Fun(s, t, te), t

      (* letRec a corriger *)
    (* | Mml.LetRec(x,t, e1, e2) ->   (*check type e1 = type e2 = t  *)
      let te1 = type_expr e1 env in
      let te2 = type_expr e2 (Smap.add x te1.t env) in
      check te1.t t;
      check te2.t t;
      Tml.LetRec(x,t, te1, te2),t *)

    (* | Mml.Pair(e0,e1)-> 
      let te0 = type_expr e0 env in
      let te1 = type_expr e1 env in  
      Tml.Pair(te0,te1), te0.t  *)

    | Mml.Fst(e)-> let te = type_expr e env in 
          (* check te.t Mml.Pair ; *)
          Tml.Fst( te), te.t
    | Mml.Snd(e)-> let te = type_expr e env in 
          (* check te.t Mml.Pair ; *)
          Tml.Snd( te), te.t
    | Mml.StrGet(e,s)-> let te= type_expr e env in  Tml.StrGet(te,s), te.t

    | Mml.Struct(s_list)-> 
      let tml_list= type_expr_list s_list env in 
      Tml.Struct(tml_list), 
      TInt
  in

  {
    Tml.types = prog.Mml.types;
    Tml.code = type_expr prog.Mml.code Smap.empty
  }
