open Fun
open Ops
open Imp
open Fun2clj
open Clj2imp




let translate_program prog = 
  let clj_prog = Fun2clj.translate_program prog in
  let imp_prog = Clj2imp.translate_program clj_prog in
  imp_prog


