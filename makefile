file = if
all: rebuild_func toimp tomips run

mml: mml0 run

mml0: rebuild_mmlc tofun tomips

fun: rebuild_func toimp tomips run

imp: rebuild_impc tomips run

rebuild_mmlc:
		ocamlbuild mmlc.native

rebuild_impc:
		ocamlbuild func.native

rebuild_func:
		ocamlbuild func.native



tofun:
		./mmlc.native tests/$(file).mml 
		mv tests/$(file).imp  tests/imp/$(file).imp


toimp:
		./func.native tests/$(file).fun 
		mv tests/$(file).imp  tests/imp/$(file).imp


tomips: 
		./impc.native tests/imp/$(file).imp 
		mv tests/imp/$(file).asm  tests/asm/$(file).asm

run:
		java -jar Mars4_5.jar nc tests/asm/$(file).asm
