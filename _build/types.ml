type ty =
  | TInt
  | TBool
  | TFun of ty * ty
  | TPair of ty * ty
  | TNamed of string

type namedtuple = (string * ty) list

exception TypeError of ty * ty
let check ty1 ty2 = if ty1 <> ty2 then raise (TypeError(ty1, ty2))
exception UnexpectedType of ty * string
exception UnknownStruct
    
let rec ty_to_string = function
  | TInt -> "int"
  | TBool -> "bool"
  | TFun(ty1, ty2) -> "(" ^ ty_to_string ty1 ^ " -> " ^ ty_to_string ty2 ^ ")"
  | TPair(ty1, ty2) -> "(" ^ ty_to_string ty1 ^ " * " ^ ty_to_string ty2 ^ ")"
  | TNamed s -> s
