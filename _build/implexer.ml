# 1 "implexer.mll"
 

  open Lexing
  open Impparser

    let keyword_or_ident =
    let h = Hashtbl.create 18 in
    List.iter (fun (s, k) -> Hashtbl.add h s k)
      [ "putchar",  PUTCHAR;
        "if",       IF;
        "else",     ELSE;
        "while",    WHILE;
        "for",      FOR;
        "break",    BREAK;
        "continue", CONTINUE;
        "while",    WHILE;
        "true",     BOOL true;
        "false",    BOOL false;
        "var",      VAR;
        "main",     MAIN;
        "function", FUNCTION;
        "return",   RETURN;
        "sbrk",     SBRK;
      ] ;
    fun s ->
      try  Hashtbl.find h s
      with Not_found -> IDENT(s)
        

# 32 "implexer.ml"
let __ocaml_lex_tables = {
  Lexing.lex_base =
   "\000\000\222\255\223\255\225\255\226\255\227\255\228\255\229\255\
    \230\255\231\255\002\000\001\000\002\000\004\000\005\000\243\255\
    \245\255\246\255\247\255\007\000\249\255\079\000\021\000\037\000\
    \002\000\255\255\252\255\002\000\253\255\240\255\235\255\242\255\
    \237\255\241\255\239\255\233\255\232\255\004\000\253\255\254\255\
    \011\000\255\255";
  Lexing.lex_backtrk =
   "\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
    \255\255\255\255\032\000\031\000\021\000\017\000\019\000\255\255\
    \255\255\255\255\255\255\007\000\255\255\005\000\004\000\011\000\
    \001\000\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
    \255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
    \001\000\255\255";
  Lexing.lex_default =
   "\002\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
    \000\000\000\000\255\255\255\255\255\255\255\255\255\255\000\000\
    \000\000\000\000\000\000\255\255\000\000\255\255\255\255\255\255\
    \255\255\000\000\000\000\027\000\000\000\000\000\000\000\000\000\
    \000\000\000\000\000\000\000\000\000\000\039\000\000\000\000\000\
    \255\255\000\000";
  Lexing.lex_trans =
   "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
    \000\000\024\000\025\000\024\000\028\000\024\000\000\000\024\000\
    \000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
    \000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
    \024\000\012\000\024\000\000\000\000\000\015\000\011\000\035\000\
    \009\000\008\000\016\000\018\000\005\000\017\000\040\000\023\000\
    \022\000\022\000\022\000\022\000\022\000\022\000\022\000\022\000\
    \022\000\022\000\041\000\020\000\013\000\019\000\014\000\034\000\
    \033\000\032\000\030\000\031\000\029\000\022\000\022\000\022\000\
    \022\000\022\000\022\000\022\000\022\000\022\000\022\000\026\000\
    \000\000\000\000\000\000\000\000\027\000\000\000\000\000\000\000\
    \000\000\000\000\000\000\004\000\000\000\003\000\000\000\021\000\
    \000\000\021\000\021\000\021\000\021\000\021\000\021\000\021\000\
    \021\000\021\000\021\000\021\000\021\000\021\000\021\000\021\000\
    \021\000\021\000\021\000\021\000\021\000\021\000\021\000\021\000\
    \021\000\021\000\021\000\007\000\010\000\006\000\036\000\021\000\
    \021\000\021\000\021\000\021\000\021\000\021\000\021\000\021\000\
    \021\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
    \021\000\021\000\021\000\021\000\021\000\021\000\021\000\021\000\
    \021\000\021\000\021\000\021\000\021\000\021\000\021\000\021\000\
    \021\000\021\000\021\000\021\000\021\000\021\000\021\000\021\000\
    \021\000\021\000\000\000\000\000\000\000\000\000\021\000\000\000\
    \021\000\021\000\021\000\021\000\021\000\021\000\021\000\021\000\
    \021\000\021\000\021\000\021\000\021\000\021\000\021\000\021\000\
    \021\000\021\000\021\000\021\000\021\000\021\000\021\000\021\000\
    \021\000\021\000\000\000\000\000\000\000\000\000\000\000\000\000\
    \000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
    \000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
    \000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
    \000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
    \000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
    \000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
    \001\000\000\000\255\255\000\000\038\000\000\000\000\000\000\000\
    \000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
    \000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
    \000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
    \000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
    \000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
    \000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
    \000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
    \000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
    \000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
    ";
  Lexing.lex_check =
   "\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
    \255\255\000\000\000\000\024\000\027\000\000\000\255\255\024\000\
    \255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
    \255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
    \000\000\000\000\024\000\255\255\255\255\000\000\000\000\011\000\
    \000\000\000\000\000\000\000\000\000\000\000\000\037\000\000\000\
    \000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
    \000\000\000\000\040\000\000\000\000\000\000\000\000\000\012\000\
    \013\000\013\000\014\000\014\000\019\000\022\000\022\000\022\000\
    \022\000\022\000\022\000\022\000\022\000\022\000\022\000\023\000\
    \255\255\255\255\255\255\255\255\023\000\255\255\255\255\255\255\
    \255\255\255\255\255\255\000\000\255\255\000\000\255\255\000\000\
    \255\255\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
    \000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
    \000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
    \000\000\000\000\000\000\000\000\000\000\000\000\010\000\021\000\
    \021\000\021\000\021\000\021\000\021\000\021\000\021\000\021\000\
    \021\000\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
    \021\000\021\000\021\000\021\000\021\000\021\000\021\000\021\000\
    \021\000\021\000\021\000\021\000\021\000\021\000\021\000\021\000\
    \021\000\021\000\021\000\021\000\021\000\021\000\021\000\021\000\
    \021\000\021\000\255\255\255\255\255\255\255\255\021\000\255\255\
    \021\000\021\000\021\000\021\000\021\000\021\000\021\000\021\000\
    \021\000\021\000\021\000\021\000\021\000\021\000\021\000\021\000\
    \021\000\021\000\021\000\021\000\021\000\021\000\021\000\021\000\
    \021\000\021\000\255\255\255\255\255\255\255\255\255\255\255\255\
    \255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
    \255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
    \255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
    \255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
    \255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
    \255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
    \000\000\255\255\027\000\255\255\037\000\255\255\255\255\255\255\
    \255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
    \255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
    \255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
    \255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
    \255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
    \255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
    \255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
    \255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
    \255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
    ";
  Lexing.lex_base_code =
   "";
  Lexing.lex_backtrk_code =
   "";
  Lexing.lex_default_code =
   "";
  Lexing.lex_trans_code =
   "";
  Lexing.lex_check_code =
   "";
  Lexing.lex_code =
   "";
}

let rec token lexbuf =
   __ocaml_lex_token_rec lexbuf 0
and __ocaml_lex_token_rec lexbuf __ocaml_lex_state =
  match Lexing.engine __ocaml_lex_tables __ocaml_lex_state lexbuf with
      | 0 ->
# 38 "implexer.mll"
      ( new_line lexbuf; token lexbuf )
# 164 "implexer.ml"

  | 1 ->
# 40 "implexer.mll"
      ( token lexbuf )
# 169 "implexer.ml"

  | 2 ->
# 42 "implexer.mll"
      ( new_line lexbuf; token lexbuf )
# 174 "implexer.ml"

  | 3 ->
# 44 "implexer.mll"
      ( comment lexbuf; token lexbuf )
# 179 "implexer.ml"

  | 4 ->
let
# 45 "implexer.mll"
              n
# 185 "implexer.ml"
= Lexing.sub_lexeme lexbuf lexbuf.Lexing.lex_start_pos lexbuf.Lexing.lex_curr_pos in
# 46 "implexer.mll"
      ( CST(int_of_string n) )
# 189 "implexer.ml"

  | 5 ->
let
# 47 "implexer.mll"
             id
# 195 "implexer.ml"
= Lexing.sub_lexeme lexbuf lexbuf.Lexing.lex_start_pos lexbuf.Lexing.lex_curr_pos in
# 48 "implexer.mll"
      ( keyword_or_ident id )
# 199 "implexer.ml"

  | 6 ->
# 50 "implexer.mll"
      ( SEMI )
# 204 "implexer.ml"

  | 7 ->
# 52 "implexer.mll"
      ( SET )
# 209 "implexer.ml"

  | 8 ->
# 54 "implexer.mll"
      ( PLUS )
# 214 "implexer.ml"

  | 9 ->
# 56 "implexer.mll"
      ( MINUS )
# 219 "implexer.ml"

  | 10 ->
# 58 "implexer.mll"
      ( STAR )
# 224 "implexer.ml"

  | 11 ->
# 60 "implexer.mll"
      ( SLASH )
# 229 "implexer.ml"

  | 12 ->
# 62 "implexer.mll"
      ( PRCT )
# 234 "implexer.ml"

  | 13 ->
# 64 "implexer.mll"
      ( LSR )
# 239 "implexer.ml"

  | 14 ->
# 66 "implexer.mll"
      ( LSL )
# 244 "implexer.ml"

  | 15 ->
# 68 "implexer.mll"
      ( EQ )
# 249 "implexer.ml"

  | 16 ->
# 70 "implexer.mll"
      ( NEQ )
# 254 "implexer.ml"

  | 17 ->
# 72 "implexer.mll"
      ( LT )
# 259 "implexer.ml"

  | 18 ->
# 74 "implexer.mll"
      ( LE )
# 264 "implexer.ml"

  | 19 ->
# 76 "implexer.mll"
      ( GT )
# 269 "implexer.ml"

  | 20 ->
# 78 "implexer.mll"
      ( GE )
# 274 "implexer.ml"

  | 21 ->
# 80 "implexer.mll"
      ( NOT )
# 279 "implexer.ml"

  | 22 ->
# 82 "implexer.mll"
      ( AND )
# 284 "implexer.ml"

  | 23 ->
# 84 "implexer.mll"
      ( OR )
# 289 "implexer.ml"

  | 24 ->
# 86 "implexer.mll"
      ( LPAR )
# 294 "implexer.ml"

  | 25 ->
# 88 "implexer.mll"
      ( RPAR )
# 299 "implexer.ml"

  | 26 ->
# 90 "implexer.mll"
      ( BEGIN )
# 304 "implexer.ml"

  | 27 ->
# 92 "implexer.mll"
      ( END )
# 309 "implexer.ml"

  | 28 ->
# 94 "implexer.mll"
      ( COMMA )
# 314 "implexer.ml"

  | 29 ->
# 96 "implexer.mll"
      ( LBRACKET )
# 319 "implexer.ml"

  | 30 ->
# 98 "implexer.mll"
      ( RBRACKET )
# 324 "implexer.ml"

  | 31 ->
# 100 "implexer.mll"
      ( AMPERSAND )
# 329 "implexer.ml"

  | 32 ->
# 102 "implexer.mll"
      ( failwith ("Unknown character : " ^ (lexeme lexbuf)) )
# 334 "implexer.ml"

  | 33 ->
# 104 "implexer.mll"
      ( EOF )
# 339 "implexer.ml"

  | __ocaml_lex_state -> lexbuf.Lexing.refill_buff lexbuf;
      __ocaml_lex_token_rec lexbuf __ocaml_lex_state

and comment lexbuf =
   __ocaml_lex_comment_rec lexbuf 37
and __ocaml_lex_comment_rec lexbuf __ocaml_lex_state =
  match Lexing.engine __ocaml_lex_tables __ocaml_lex_state lexbuf with
      | 0 ->
# 108 "implexer.mll"
      ( () )
# 351 "implexer.ml"

  | 1 ->
# 110 "implexer.mll"
      ( comment lexbuf )
# 356 "implexer.ml"

  | 2 ->
# 112 "implexer.mll"
      ( failwith "unfinished comment" )
# 361 "implexer.ml"

  | __ocaml_lex_state -> lexbuf.Lexing.refill_buff lexbuf;
      __ocaml_lex_comment_rec lexbuf __ocaml_lex_state

;;

