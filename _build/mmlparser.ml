
module MenhirBasics = struct
  
  exception Error
  
  type token = 
    | TYPE
    | TINT
    | THEN
    | TBOOL
    | STAR
    | SND
    | SLASH
    | SEMI
    | RPAR
    | REC
    | RBRACE
    | PLUS
    | OR
    | NOT
    | NEQ
    | MOD
    | MINUS
    | LT
    | LSR
    | LSL
    | LPAR
    | LET
    | LE
    | LBRACE
    | IN
    | IF
    | IDENT of (
# 16 "mmlparser.mly"
       (string)
# 37 "mmlparser.ml"
  )
    | GT
    | GE
    | FUN
    | FST
    | EQ
    | EOF
    | ELSE
    | DOT
    | CST of (
# 14 "mmlparser.mly"
       (int)
# 50 "mmlparser.ml"
  )
    | COMMA
    | COLON
    | BOOL of (
# 15 "mmlparser.mly"
       (bool)
# 57 "mmlparser.ml"
  )
    | ARROW
    | AND
  
end

include MenhirBasics

let _eRR =
  MenhirBasics.Error

type _menhir_env = {
  _menhir_lexer: Lexing.lexbuf -> token;
  _menhir_lexbuf: Lexing.lexbuf;
  _menhir_token: token;
  mutable _menhir_error: bool
}

and _menhir_state = 
  | MenhirState124
  | MenhirState123
  | MenhirState121
  | MenhirState119
  | MenhirState118
  | MenhirState116
  | MenhirState115
  | MenhirState114
  | MenhirState113
  | MenhirState112
  | MenhirState110
  | MenhirState109
  | MenhirState108
  | MenhirState107
  | MenhirState105
  | MenhirState100
  | MenhirState99
  | MenhirState98
  | MenhirState97
  | MenhirState96
  | MenhirState95
  | MenhirState94
  | MenhirState93
  | MenhirState92
  | MenhirState91
  | MenhirState90
  | MenhirState89
  | MenhirState88
  | MenhirState87
  | MenhirState86
  | MenhirState85
  | MenhirState84
  | MenhirState83
  | MenhirState82
  | MenhirState81
  | MenhirState80
  | MenhirState79
  | MenhirState78
  | MenhirState77
  | MenhirState76
  | MenhirState75
  | MenhirState74
  | MenhirState73
  | MenhirState72
  | MenhirState71
  | MenhirState70
  | MenhirState69
  | MenhirState68
  | MenhirState67
  | MenhirState66
  | MenhirState65
  | MenhirState64
  | MenhirState62
  | MenhirState60
  | MenhirState59
  | MenhirState56
  | MenhirState54
  | MenhirState53
  | MenhirState51
  | MenhirState50
  | MenhirState48
  | MenhirState45
  | MenhirState44
  | MenhirState41
  | MenhirState34
  | MenhirState33
  | MenhirState32
  | MenhirState31
  | MenhirState30
  | MenhirState27
  | MenhirState22
  | MenhirState16
  | MenhirState13
  | MenhirState10
  | MenhirState7
  | MenhirState5
  | MenhirState4
  | MenhirState0

# 1 "mmlparser.mly"
  

  open Lexing
  open Mml
  open Types
  open Ops


# 165 "mmlparser.ml"

let rec _menhir_run117 : _menhir_env -> ('ttv_tail * _menhir_state) * _menhir_state * (Mml.expression) -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_env = _menhir_discard _menhir_env in
    let _menhir_stack = Obj.magic _menhir_stack in
    let ((_menhir_stack, _menhir_s), _, (e : (Mml.expression))) = _menhir_stack in
    let _3 = () in
    let _1 = () in
    let _v : (Mml.expression) = 
# 74 "mmlparser.mly"
                         ( e )
# 177 "mmlparser.ml"
     in
    _menhir_goto_simple_expression _menhir_env _menhir_stack _menhir_s _v

and _menhir_goto_separated_nonempty_list_SEMI_field_def_ : _menhir_env -> 'ttv_tail -> _menhir_state -> ((string * Mml.expression) list) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    match _menhir_s with
    | MenhirState51 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let (x : ((string * Mml.expression) list)) = _v in
        let _v : ((string * Mml.expression) list) = 
# 144 "/home/mohamedh/.opam/system/lib/menhir/standard.mly"
    ( x )
# 191 "mmlparser.ml"
         in
        _menhir_goto_loption_separated_nonempty_list_SEMI_field_def__ _menhir_env _menhir_stack _menhir_s _v
    | MenhirState105 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let (xs : ((string * Mml.expression) list)) = _v in
        let (_menhir_stack, _menhir_s, (x : (string * Mml.expression))) = _menhir_stack in
        let _2 = () in
        let _v : ((string * Mml.expression) list) = 
# 231 "/home/mohamedh/.opam/system/lib/menhir/standard.mly"
    ( x :: xs )
# 203 "mmlparser.ml"
         in
        _menhir_goto_separated_nonempty_list_SEMI_field_def_ _menhir_env _menhir_stack _menhir_s _v
    | _ ->
        _menhir_fail ()

and _menhir_run65 : _menhir_env -> 'ttv_tail * _menhir_state * (Mml.expression) -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_stack = (_menhir_stack, _menhir_s) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | BOOL _v ->
        _menhir_run37 _menhir_env (Obj.magic _menhir_stack) MenhirState65 _v
    | CST _v ->
        _menhir_run36 _menhir_env (Obj.magic _menhir_stack) MenhirState65 _v
    | FST ->
        _menhir_run60 _menhir_env (Obj.magic _menhir_stack) MenhirState65
    | FUN ->
        _menhir_run55 _menhir_env (Obj.magic _menhir_stack) MenhirState65
    | IDENT _v ->
        _menhir_run35 _menhir_env (Obj.magic _menhir_stack) MenhirState65 _v
    | IF ->
        _menhir_run54 _menhir_env (Obj.magic _menhir_stack) MenhirState65
    | LBRACE ->
        _menhir_run51 _menhir_env (Obj.magic _menhir_stack) MenhirState65
    | LET ->
        _menhir_run42 _menhir_env (Obj.magic _menhir_stack) MenhirState65
    | LPAR ->
        _menhir_run41 _menhir_env (Obj.magic _menhir_stack) MenhirState65
    | MINUS ->
        _menhir_run34 _menhir_env (Obj.magic _menhir_stack) MenhirState65
    | NOT ->
        _menhir_run32 _menhir_env (Obj.magic _menhir_stack) MenhirState65
    | SND ->
        _menhir_run31 _menhir_env (Obj.magic _menhir_stack) MenhirState65
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState65

and _menhir_run71 : _menhir_env -> 'ttv_tail * _menhir_state * (Mml.expression) -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_stack = (_menhir_stack, _menhir_s) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | BOOL _v ->
        _menhir_run37 _menhir_env (Obj.magic _menhir_stack) MenhirState71 _v
    | CST _v ->
        _menhir_run36 _menhir_env (Obj.magic _menhir_stack) MenhirState71 _v
    | FST ->
        _menhir_run60 _menhir_env (Obj.magic _menhir_stack) MenhirState71
    | FUN ->
        _menhir_run55 _menhir_env (Obj.magic _menhir_stack) MenhirState71
    | IDENT _v ->
        _menhir_run35 _menhir_env (Obj.magic _menhir_stack) MenhirState71 _v
    | IF ->
        _menhir_run54 _menhir_env (Obj.magic _menhir_stack) MenhirState71
    | LBRACE ->
        _menhir_run51 _menhir_env (Obj.magic _menhir_stack) MenhirState71
    | LET ->
        _menhir_run42 _menhir_env (Obj.magic _menhir_stack) MenhirState71
    | LPAR ->
        _menhir_run41 _menhir_env (Obj.magic _menhir_stack) MenhirState71
    | MINUS ->
        _menhir_run34 _menhir_env (Obj.magic _menhir_stack) MenhirState71
    | NOT ->
        _menhir_run32 _menhir_env (Obj.magic _menhir_stack) MenhirState71
    | SND ->
        _menhir_run31 _menhir_env (Obj.magic _menhir_stack) MenhirState71
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState71

and _menhir_run73 : _menhir_env -> 'ttv_tail * _menhir_state * (Mml.expression) -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_stack = (_menhir_stack, _menhir_s) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | BOOL _v ->
        _menhir_run37 _menhir_env (Obj.magic _menhir_stack) MenhirState73 _v
    | CST _v ->
        _menhir_run36 _menhir_env (Obj.magic _menhir_stack) MenhirState73 _v
    | FST ->
        _menhir_run60 _menhir_env (Obj.magic _menhir_stack) MenhirState73
    | FUN ->
        _menhir_run55 _menhir_env (Obj.magic _menhir_stack) MenhirState73
    | IDENT _v ->
        _menhir_run35 _menhir_env (Obj.magic _menhir_stack) MenhirState73 _v
    | IF ->
        _menhir_run54 _menhir_env (Obj.magic _menhir_stack) MenhirState73
    | LBRACE ->
        _menhir_run51 _menhir_env (Obj.magic _menhir_stack) MenhirState73
    | LET ->
        _menhir_run42 _menhir_env (Obj.magic _menhir_stack) MenhirState73
    | LPAR ->
        _menhir_run41 _menhir_env (Obj.magic _menhir_stack) MenhirState73
    | MINUS ->
        _menhir_run34 _menhir_env (Obj.magic _menhir_stack) MenhirState73
    | NOT ->
        _menhir_run32 _menhir_env (Obj.magic _menhir_stack) MenhirState73
    | SND ->
        _menhir_run31 _menhir_env (Obj.magic _menhir_stack) MenhirState73
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState73

and _menhir_run77 : _menhir_env -> 'ttv_tail * _menhir_state * (Mml.expression) -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_stack = (_menhir_stack, _menhir_s) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | BOOL _v ->
        _menhir_run37 _menhir_env (Obj.magic _menhir_stack) MenhirState77 _v
    | CST _v ->
        _menhir_run36 _menhir_env (Obj.magic _menhir_stack) MenhirState77 _v
    | FST ->
        _menhir_run60 _menhir_env (Obj.magic _menhir_stack) MenhirState77
    | FUN ->
        _menhir_run55 _menhir_env (Obj.magic _menhir_stack) MenhirState77
    | IDENT _v ->
        _menhir_run35 _menhir_env (Obj.magic _menhir_stack) MenhirState77 _v
    | IF ->
        _menhir_run54 _menhir_env (Obj.magic _menhir_stack) MenhirState77
    | LBRACE ->
        _menhir_run51 _menhir_env (Obj.magic _menhir_stack) MenhirState77
    | LET ->
        _menhir_run42 _menhir_env (Obj.magic _menhir_stack) MenhirState77
    | LPAR ->
        _menhir_run41 _menhir_env (Obj.magic _menhir_stack) MenhirState77
    | MINUS ->
        _menhir_run34 _menhir_env (Obj.magic _menhir_stack) MenhirState77
    | NOT ->
        _menhir_run32 _menhir_env (Obj.magic _menhir_stack) MenhirState77
    | SND ->
        _menhir_run31 _menhir_env (Obj.magic _menhir_stack) MenhirState77
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState77

and _menhir_run79 : _menhir_env -> 'ttv_tail * _menhir_state * (Mml.expression) -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_stack = (_menhir_stack, _menhir_s) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | BOOL _v ->
        _menhir_run37 _menhir_env (Obj.magic _menhir_stack) MenhirState79 _v
    | CST _v ->
        _menhir_run36 _menhir_env (Obj.magic _menhir_stack) MenhirState79 _v
    | FST ->
        _menhir_run60 _menhir_env (Obj.magic _menhir_stack) MenhirState79
    | FUN ->
        _menhir_run55 _menhir_env (Obj.magic _menhir_stack) MenhirState79
    | IDENT _v ->
        _menhir_run35 _menhir_env (Obj.magic _menhir_stack) MenhirState79 _v
    | IF ->
        _menhir_run54 _menhir_env (Obj.magic _menhir_stack) MenhirState79
    | LBRACE ->
        _menhir_run51 _menhir_env (Obj.magic _menhir_stack) MenhirState79
    | LET ->
        _menhir_run42 _menhir_env (Obj.magic _menhir_stack) MenhirState79
    | LPAR ->
        _menhir_run41 _menhir_env (Obj.magic _menhir_stack) MenhirState79
    | MINUS ->
        _menhir_run34 _menhir_env (Obj.magic _menhir_stack) MenhirState79
    | NOT ->
        _menhir_run32 _menhir_env (Obj.magic _menhir_stack) MenhirState79
    | SND ->
        _menhir_run31 _menhir_env (Obj.magic _menhir_stack) MenhirState79
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState79

and _menhir_run75 : _menhir_env -> 'ttv_tail * _menhir_state * (Mml.expression) -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_stack = (_menhir_stack, _menhir_s) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | BOOL _v ->
        _menhir_run37 _menhir_env (Obj.magic _menhir_stack) MenhirState75 _v
    | CST _v ->
        _menhir_run36 _menhir_env (Obj.magic _menhir_stack) MenhirState75 _v
    | FST ->
        _menhir_run60 _menhir_env (Obj.magic _menhir_stack) MenhirState75
    | FUN ->
        _menhir_run55 _menhir_env (Obj.magic _menhir_stack) MenhirState75
    | IDENT _v ->
        _menhir_run35 _menhir_env (Obj.magic _menhir_stack) MenhirState75 _v
    | IF ->
        _menhir_run54 _menhir_env (Obj.magic _menhir_stack) MenhirState75
    | LBRACE ->
        _menhir_run51 _menhir_env (Obj.magic _menhir_stack) MenhirState75
    | LET ->
        _menhir_run42 _menhir_env (Obj.magic _menhir_stack) MenhirState75
    | LPAR ->
        _menhir_run41 _menhir_env (Obj.magic _menhir_stack) MenhirState75
    | MINUS ->
        _menhir_run34 _menhir_env (Obj.magic _menhir_stack) MenhirState75
    | NOT ->
        _menhir_run32 _menhir_env (Obj.magic _menhir_stack) MenhirState75
    | SND ->
        _menhir_run31 _menhir_env (Obj.magic _menhir_stack) MenhirState75
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState75

and _menhir_run81 : _menhir_env -> 'ttv_tail * _menhir_state * (Mml.expression) -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_stack = (_menhir_stack, _menhir_s) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | BOOL _v ->
        _menhir_run37 _menhir_env (Obj.magic _menhir_stack) MenhirState81 _v
    | CST _v ->
        _menhir_run36 _menhir_env (Obj.magic _menhir_stack) MenhirState81 _v
    | FST ->
        _menhir_run60 _menhir_env (Obj.magic _menhir_stack) MenhirState81
    | FUN ->
        _menhir_run55 _menhir_env (Obj.magic _menhir_stack) MenhirState81
    | IDENT _v ->
        _menhir_run35 _menhir_env (Obj.magic _menhir_stack) MenhirState81 _v
    | IF ->
        _menhir_run54 _menhir_env (Obj.magic _menhir_stack) MenhirState81
    | LBRACE ->
        _menhir_run51 _menhir_env (Obj.magic _menhir_stack) MenhirState81
    | LET ->
        _menhir_run42 _menhir_env (Obj.magic _menhir_stack) MenhirState81
    | LPAR ->
        _menhir_run41 _menhir_env (Obj.magic _menhir_stack) MenhirState81
    | MINUS ->
        _menhir_run34 _menhir_env (Obj.magic _menhir_stack) MenhirState81
    | NOT ->
        _menhir_run32 _menhir_env (Obj.magic _menhir_stack) MenhirState81
    | SND ->
        _menhir_run31 _menhir_env (Obj.magic _menhir_stack) MenhirState81
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState81

and _menhir_run83 : _menhir_env -> 'ttv_tail * _menhir_state * (Mml.expression) -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_stack = (_menhir_stack, _menhir_s) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | BOOL _v ->
        _menhir_run37 _menhir_env (Obj.magic _menhir_stack) MenhirState83 _v
    | CST _v ->
        _menhir_run36 _menhir_env (Obj.magic _menhir_stack) MenhirState83 _v
    | FST ->
        _menhir_run60 _menhir_env (Obj.magic _menhir_stack) MenhirState83
    | FUN ->
        _menhir_run55 _menhir_env (Obj.magic _menhir_stack) MenhirState83
    | IDENT _v ->
        _menhir_run35 _menhir_env (Obj.magic _menhir_stack) MenhirState83 _v
    | IF ->
        _menhir_run54 _menhir_env (Obj.magic _menhir_stack) MenhirState83
    | LBRACE ->
        _menhir_run51 _menhir_env (Obj.magic _menhir_stack) MenhirState83
    | LET ->
        _menhir_run42 _menhir_env (Obj.magic _menhir_stack) MenhirState83
    | LPAR ->
        _menhir_run41 _menhir_env (Obj.magic _menhir_stack) MenhirState83
    | MINUS ->
        _menhir_run34 _menhir_env (Obj.magic _menhir_stack) MenhirState83
    | NOT ->
        _menhir_run32 _menhir_env (Obj.magic _menhir_stack) MenhirState83
    | SND ->
        _menhir_run31 _menhir_env (Obj.magic _menhir_stack) MenhirState83
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState83

and _menhir_run67 : _menhir_env -> 'ttv_tail * _menhir_state * (Mml.expression) -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_stack = (_menhir_stack, _menhir_s) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | BOOL _v ->
        _menhir_run37 _menhir_env (Obj.magic _menhir_stack) MenhirState67 _v
    | CST _v ->
        _menhir_run36 _menhir_env (Obj.magic _menhir_stack) MenhirState67 _v
    | FST ->
        _menhir_run60 _menhir_env (Obj.magic _menhir_stack) MenhirState67
    | FUN ->
        _menhir_run55 _menhir_env (Obj.magic _menhir_stack) MenhirState67
    | IDENT _v ->
        _menhir_run35 _menhir_env (Obj.magic _menhir_stack) MenhirState67 _v
    | IF ->
        _menhir_run54 _menhir_env (Obj.magic _menhir_stack) MenhirState67
    | LBRACE ->
        _menhir_run51 _menhir_env (Obj.magic _menhir_stack) MenhirState67
    | LET ->
        _menhir_run42 _menhir_env (Obj.magic _menhir_stack) MenhirState67
    | LPAR ->
        _menhir_run41 _menhir_env (Obj.magic _menhir_stack) MenhirState67
    | MINUS ->
        _menhir_run34 _menhir_env (Obj.magic _menhir_stack) MenhirState67
    | NOT ->
        _menhir_run32 _menhir_env (Obj.magic _menhir_stack) MenhirState67
    | SND ->
        _menhir_run31 _menhir_env (Obj.magic _menhir_stack) MenhirState67
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState67

and _menhir_run69 : _menhir_env -> 'ttv_tail * _menhir_state * (Mml.expression) -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_stack = (_menhir_stack, _menhir_s) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | BOOL _v ->
        _menhir_run37 _menhir_env (Obj.magic _menhir_stack) MenhirState69 _v
    | CST _v ->
        _menhir_run36 _menhir_env (Obj.magic _menhir_stack) MenhirState69 _v
    | FST ->
        _menhir_run60 _menhir_env (Obj.magic _menhir_stack) MenhirState69
    | FUN ->
        _menhir_run55 _menhir_env (Obj.magic _menhir_stack) MenhirState69
    | IDENT _v ->
        _menhir_run35 _menhir_env (Obj.magic _menhir_stack) MenhirState69 _v
    | IF ->
        _menhir_run54 _menhir_env (Obj.magic _menhir_stack) MenhirState69
    | LBRACE ->
        _menhir_run51 _menhir_env (Obj.magic _menhir_stack) MenhirState69
    | LET ->
        _menhir_run42 _menhir_env (Obj.magic _menhir_stack) MenhirState69
    | LPAR ->
        _menhir_run41 _menhir_env (Obj.magic _menhir_stack) MenhirState69
    | MINUS ->
        _menhir_run34 _menhir_env (Obj.magic _menhir_stack) MenhirState69
    | NOT ->
        _menhir_run32 _menhir_env (Obj.magic _menhir_stack) MenhirState69
    | SND ->
        _menhir_run31 _menhir_env (Obj.magic _menhir_stack) MenhirState69
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState69

and _menhir_run85 : _menhir_env -> 'ttv_tail * _menhir_state * (Mml.expression) -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_stack = (_menhir_stack, _menhir_s) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | BOOL _v ->
        _menhir_run37 _menhir_env (Obj.magic _menhir_stack) MenhirState85 _v
    | CST _v ->
        _menhir_run36 _menhir_env (Obj.magic _menhir_stack) MenhirState85 _v
    | FST ->
        _menhir_run60 _menhir_env (Obj.magic _menhir_stack) MenhirState85
    | FUN ->
        _menhir_run55 _menhir_env (Obj.magic _menhir_stack) MenhirState85
    | IDENT _v ->
        _menhir_run35 _menhir_env (Obj.magic _menhir_stack) MenhirState85 _v
    | IF ->
        _menhir_run54 _menhir_env (Obj.magic _menhir_stack) MenhirState85
    | LBRACE ->
        _menhir_run51 _menhir_env (Obj.magic _menhir_stack) MenhirState85
    | LET ->
        _menhir_run42 _menhir_env (Obj.magic _menhir_stack) MenhirState85
    | LPAR ->
        _menhir_run41 _menhir_env (Obj.magic _menhir_stack) MenhirState85
    | MINUS ->
        _menhir_run34 _menhir_env (Obj.magic _menhir_stack) MenhirState85
    | NOT ->
        _menhir_run32 _menhir_env (Obj.magic _menhir_stack) MenhirState85
    | SND ->
        _menhir_run31 _menhir_env (Obj.magic _menhir_stack) MenhirState85
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState85

and _menhir_run87 : _menhir_env -> 'ttv_tail * _menhir_state * (Mml.expression) -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_stack = (_menhir_stack, _menhir_s) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | BOOL _v ->
        _menhir_run37 _menhir_env (Obj.magic _menhir_stack) MenhirState87 _v
    | CST _v ->
        _menhir_run36 _menhir_env (Obj.magic _menhir_stack) MenhirState87 _v
    | FST ->
        _menhir_run60 _menhir_env (Obj.magic _menhir_stack) MenhirState87
    | FUN ->
        _menhir_run55 _menhir_env (Obj.magic _menhir_stack) MenhirState87
    | IDENT _v ->
        _menhir_run35 _menhir_env (Obj.magic _menhir_stack) MenhirState87 _v
    | IF ->
        _menhir_run54 _menhir_env (Obj.magic _menhir_stack) MenhirState87
    | LBRACE ->
        _menhir_run51 _menhir_env (Obj.magic _menhir_stack) MenhirState87
    | LET ->
        _menhir_run42 _menhir_env (Obj.magic _menhir_stack) MenhirState87
    | LPAR ->
        _menhir_run41 _menhir_env (Obj.magic _menhir_stack) MenhirState87
    | MINUS ->
        _menhir_run34 _menhir_env (Obj.magic _menhir_stack) MenhirState87
    | NOT ->
        _menhir_run32 _menhir_env (Obj.magic _menhir_stack) MenhirState87
    | SND ->
        _menhir_run31 _menhir_env (Obj.magic _menhir_stack) MenhirState87
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState87

and _menhir_run89 : _menhir_env -> 'ttv_tail * _menhir_state * (Mml.expression) -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_stack = (_menhir_stack, _menhir_s) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | BOOL _v ->
        _menhir_run37 _menhir_env (Obj.magic _menhir_stack) MenhirState89 _v
    | CST _v ->
        _menhir_run36 _menhir_env (Obj.magic _menhir_stack) MenhirState89 _v
    | FST ->
        _menhir_run60 _menhir_env (Obj.magic _menhir_stack) MenhirState89
    | FUN ->
        _menhir_run55 _menhir_env (Obj.magic _menhir_stack) MenhirState89
    | IDENT _v ->
        _menhir_run35 _menhir_env (Obj.magic _menhir_stack) MenhirState89 _v
    | IF ->
        _menhir_run54 _menhir_env (Obj.magic _menhir_stack) MenhirState89
    | LBRACE ->
        _menhir_run51 _menhir_env (Obj.magic _menhir_stack) MenhirState89
    | LET ->
        _menhir_run42 _menhir_env (Obj.magic _menhir_stack) MenhirState89
    | LPAR ->
        _menhir_run41 _menhir_env (Obj.magic _menhir_stack) MenhirState89
    | MINUS ->
        _menhir_run34 _menhir_env (Obj.magic _menhir_stack) MenhirState89
    | NOT ->
        _menhir_run32 _menhir_env (Obj.magic _menhir_stack) MenhirState89
    | SND ->
        _menhir_run31 _menhir_env (Obj.magic _menhir_stack) MenhirState89
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState89

and _menhir_run91 : _menhir_env -> 'ttv_tail * _menhir_state * (Mml.expression) -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_stack = (_menhir_stack, _menhir_s) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | BOOL _v ->
        _menhir_run37 _menhir_env (Obj.magic _menhir_stack) MenhirState91 _v
    | CST _v ->
        _menhir_run36 _menhir_env (Obj.magic _menhir_stack) MenhirState91 _v
    | FST ->
        _menhir_run60 _menhir_env (Obj.magic _menhir_stack) MenhirState91
    | FUN ->
        _menhir_run55 _menhir_env (Obj.magic _menhir_stack) MenhirState91
    | IDENT _v ->
        _menhir_run35 _menhir_env (Obj.magic _menhir_stack) MenhirState91 _v
    | IF ->
        _menhir_run54 _menhir_env (Obj.magic _menhir_stack) MenhirState91
    | LBRACE ->
        _menhir_run51 _menhir_env (Obj.magic _menhir_stack) MenhirState91
    | LET ->
        _menhir_run42 _menhir_env (Obj.magic _menhir_stack) MenhirState91
    | LPAR ->
        _menhir_run41 _menhir_env (Obj.magic _menhir_stack) MenhirState91
    | MINUS ->
        _menhir_run34 _menhir_env (Obj.magic _menhir_stack) MenhirState91
    | NOT ->
        _menhir_run32 _menhir_env (Obj.magic _menhir_stack) MenhirState91
    | SND ->
        _menhir_run31 _menhir_env (Obj.magic _menhir_stack) MenhirState91
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState91

and _menhir_run93 : _menhir_env -> 'ttv_tail * _menhir_state * (Mml.expression) -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_stack = (_menhir_stack, _menhir_s) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | BOOL _v ->
        _menhir_run37 _menhir_env (Obj.magic _menhir_stack) MenhirState93 _v
    | CST _v ->
        _menhir_run36 _menhir_env (Obj.magic _menhir_stack) MenhirState93 _v
    | FST ->
        _menhir_run60 _menhir_env (Obj.magic _menhir_stack) MenhirState93
    | FUN ->
        _menhir_run55 _menhir_env (Obj.magic _menhir_stack) MenhirState93
    | IDENT _v ->
        _menhir_run35 _menhir_env (Obj.magic _menhir_stack) MenhirState93 _v
    | IF ->
        _menhir_run54 _menhir_env (Obj.magic _menhir_stack) MenhirState93
    | LBRACE ->
        _menhir_run51 _menhir_env (Obj.magic _menhir_stack) MenhirState93
    | LET ->
        _menhir_run42 _menhir_env (Obj.magic _menhir_stack) MenhirState93
    | LPAR ->
        _menhir_run41 _menhir_env (Obj.magic _menhir_stack) MenhirState93
    | MINUS ->
        _menhir_run34 _menhir_env (Obj.magic _menhir_stack) MenhirState93
    | NOT ->
        _menhir_run32 _menhir_env (Obj.magic _menhir_stack) MenhirState93
    | SND ->
        _menhir_run31 _menhir_env (Obj.magic _menhir_stack) MenhirState93
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState93

and _menhir_goto_separated_nonempty_list_SEMI_typed_ident_ : _menhir_env -> 'ttv_tail -> _menhir_state -> (Types.namedtuple) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    match _menhir_s with
    | MenhirState22 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let (xs : (Types.namedtuple)) = _v in
        let (_menhir_stack, _menhir_s, (x : (string * Types.ty))) = _menhir_stack in
        let _2 = () in
        let _v : (Types.namedtuple) = 
# 231 "/home/mohamedh/.opam/system/lib/menhir/standard.mly"
    ( x :: xs )
# 746 "mmlparser.ml"
         in
        _menhir_goto_separated_nonempty_list_SEMI_typed_ident_ _menhir_env _menhir_stack _menhir_s _v
    | MenhirState4 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let (x : (Types.namedtuple)) = _v in
        let _v : (Types.namedtuple) = 
# 144 "/home/mohamedh/.opam/system/lib/menhir/standard.mly"
    ( x )
# 756 "mmlparser.ml"
         in
        _menhir_goto_loption_separated_nonempty_list_SEMI_typed_ident__ _menhir_env _menhir_stack _menhir_s _v
    | _ ->
        _menhir_fail ()

and _menhir_goto_list_typed_ident_ : _menhir_env -> 'ttv_tail -> _menhir_state -> ((string * Types.ty) list) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    let _menhir_stack = (_menhir_stack, _menhir_s, _v) in
    match _menhir_s with
    | MenhirState45 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let ((_menhir_stack, _menhir_s, (x : (string * Types.ty))), _, (xs : ((string * Types.ty) list))) = _menhir_stack in
        let _v : ((string * Types.ty) list) = 
# 201 "/home/mohamedh/.opam/system/lib/menhir/standard.mly"
    ( x :: xs )
# 773 "mmlparser.ml"
         in
        _menhir_goto_list_typed_ident_ _menhir_env _menhir_stack _menhir_s _v
    | MenhirState44 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | COLON ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_env = _menhir_discard _menhir_env in
            let _tok = _menhir_env._menhir_token in
            (match _tok with
            | IDENT _v ->
                _menhir_run11 _menhir_env (Obj.magic _menhir_stack) MenhirState48 _v
            | LPAR ->
                _menhir_run10 _menhir_env (Obj.magic _menhir_stack) MenhirState48
            | TBOOL ->
                _menhir_run9 _menhir_env (Obj.magic _menhir_stack) MenhirState48
            | TINT ->
                _menhir_run8 _menhir_env (Obj.magic _menhir_stack) MenhirState48
            | _ ->
                assert (not _menhir_env._menhir_error);
                _menhir_env._menhir_error <- true;
                _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState48)
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState110 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | EQ ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_env = _menhir_discard _menhir_env in
            let _tok = _menhir_env._menhir_token in
            (match _tok with
            | BOOL _v ->
                _menhir_run37 _menhir_env (Obj.magic _menhir_stack) MenhirState112 _v
            | CST _v ->
                _menhir_run36 _menhir_env (Obj.magic _menhir_stack) MenhirState112 _v
            | FST ->
                _menhir_run60 _menhir_env (Obj.magic _menhir_stack) MenhirState112
            | FUN ->
                _menhir_run55 _menhir_env (Obj.magic _menhir_stack) MenhirState112
            | IDENT _v ->
                _menhir_run35 _menhir_env (Obj.magic _menhir_stack) MenhirState112 _v
            | IF ->
                _menhir_run54 _menhir_env (Obj.magic _menhir_stack) MenhirState112
            | LBRACE ->
                _menhir_run51 _menhir_env (Obj.magic _menhir_stack) MenhirState112
            | LET ->
                _menhir_run42 _menhir_env (Obj.magic _menhir_stack) MenhirState112
            | LPAR ->
                _menhir_run41 _menhir_env (Obj.magic _menhir_stack) MenhirState112
            | MINUS ->
                _menhir_run34 _menhir_env (Obj.magic _menhir_stack) MenhirState112
            | NOT ->
                _menhir_run32 _menhir_env (Obj.magic _menhir_stack) MenhirState112
            | SND ->
                _menhir_run31 _menhir_env (Obj.magic _menhir_stack) MenhirState112
            | _ ->
                assert (not _menhir_env._menhir_error);
                _menhir_env._menhir_error <- true;
                _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState112)
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | _ ->
        _menhir_fail ()

and _menhir_goto_expression : _menhir_env -> 'ttv_tail -> _menhir_state -> (Mml.expression) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    let _menhir_stack = (_menhir_stack, _menhir_s, _v) in
    match _menhir_s with
    | MenhirState60 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | BOOL _v ->
            _menhir_run37 _menhir_env (Obj.magic _menhir_stack) MenhirState62 _v
        | CST _v ->
            _menhir_run36 _menhir_env (Obj.magic _menhir_stack) MenhirState62 _v
        | IDENT _v ->
            _menhir_run35 _menhir_env (Obj.magic _menhir_stack) MenhirState62 _v
        | LPAR ->
            _menhir_run33 _menhir_env (Obj.magic _menhir_stack) MenhirState62
        | AND | COMMA | ELSE | EOF | EQ | GE | GT | IN | LE | LSL | LSR | LT | MINUS | MOD | NEQ | OR | PLUS | RBRACE | RPAR | SEMI | SLASH | STAR | THEN ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let ((_menhir_stack, _menhir_s), _, (e : (Mml.expression))) = _menhir_stack in
            let _1 = () in
            let _v : (Mml.expression) = 
# 82 "mmlparser.mly"
                   ( Fst(e) )
# 875 "mmlparser.ml"
             in
            _menhir_goto_expression _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState62)
    | MenhirState59 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | AND ->
            _menhir_run93 _menhir_env (Obj.magic _menhir_stack) MenhirState64
        | BOOL _v ->
            _menhir_run37 _menhir_env (Obj.magic _menhir_stack) MenhirState64 _v
        | CST _v ->
            _menhir_run36 _menhir_env (Obj.magic _menhir_stack) MenhirState64 _v
        | EQ ->
            _menhir_run91 _menhir_env (Obj.magic _menhir_stack) MenhirState64
        | GE ->
            _menhir_run89 _menhir_env (Obj.magic _menhir_stack) MenhirState64
        | GT ->
            _menhir_run87 _menhir_env (Obj.magic _menhir_stack) MenhirState64
        | IDENT _v ->
            _menhir_run35 _menhir_env (Obj.magic _menhir_stack) MenhirState64 _v
        | LE ->
            _menhir_run85 _menhir_env (Obj.magic _menhir_stack) MenhirState64
        | LPAR ->
            _menhir_run33 _menhir_env (Obj.magic _menhir_stack) MenhirState64
        | LSL ->
            _menhir_run69 _menhir_env (Obj.magic _menhir_stack) MenhirState64
        | LSR ->
            _menhir_run67 _menhir_env (Obj.magic _menhir_stack) MenhirState64
        | LT ->
            _menhir_run83 _menhir_env (Obj.magic _menhir_stack) MenhirState64
        | MINUS ->
            _menhir_run81 _menhir_env (Obj.magic _menhir_stack) MenhirState64
        | MOD ->
            _menhir_run75 _menhir_env (Obj.magic _menhir_stack) MenhirState64
        | NEQ ->
            _menhir_run79 _menhir_env (Obj.magic _menhir_stack) MenhirState64
        | OR ->
            _menhir_run77 _menhir_env (Obj.magic _menhir_stack) MenhirState64
        | PLUS ->
            _menhir_run73 _menhir_env (Obj.magic _menhir_stack) MenhirState64
        | SLASH ->
            _menhir_run71 _menhir_env (Obj.magic _menhir_stack) MenhirState64
        | STAR ->
            _menhir_run65 _menhir_env (Obj.magic _menhir_stack) MenhirState64
        | COMMA | ELSE | EOF | IN | RBRACE | RPAR | SEMI | THEN ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let (((_menhir_stack, _menhir_s), _, (tid : (string * Types.ty))), _, (e : (Mml.expression))) = _menhir_stack in
            let _5 = () in
            let _4 = () in
            let _2 = () in
            let _1 = () in
            let _v : (Mml.expression) = 
# 86 "mmlparser.mly"
                                                   ( let x,t = tid in Fun(x, t, e) )
# 935 "mmlparser.ml"
             in
            _menhir_goto_expression _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState64)
    | MenhirState65 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | BOOL _v ->
            _menhir_run37 _menhir_env (Obj.magic _menhir_stack) MenhirState66 _v
        | CST _v ->
            _menhir_run36 _menhir_env (Obj.magic _menhir_stack) MenhirState66 _v
        | IDENT _v ->
            _menhir_run35 _menhir_env (Obj.magic _menhir_stack) MenhirState66 _v
        | LPAR ->
            _menhir_run33 _menhir_env (Obj.magic _menhir_stack) MenhirState66
        | LSL ->
            _menhir_run69 _menhir_env (Obj.magic _menhir_stack) MenhirState66
        | LSR ->
            _menhir_run67 _menhir_env (Obj.magic _menhir_stack) MenhirState66
        | AND | COMMA | ELSE | EOF | EQ | GE | GT | IN | LE | LT | MINUS | MOD | NEQ | OR | PLUS | RBRACE | RPAR | SEMI | SLASH | STAR | THEN ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let (((_menhir_stack, _menhir_s, (e1 : (Mml.expression))), _), _, (e2 : (Mml.expression))) = _menhir_stack in
            let _10 = () in
            let _v : (Mml.expression) = let op =
              let _1 = _10 in
              
# 103 "mmlparser.mly"
       ( Mul )
# 968 "mmlparser.ml"
              
            in
            
# 80 "mmlparser.mly"
                                       ( Binop(op, e1, e2) )
# 974 "mmlparser.ml"
             in
            _menhir_goto_expression _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState66)
    | MenhirState67 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | BOOL _v ->
            _menhir_run37 _menhir_env (Obj.magic _menhir_stack) MenhirState68 _v
        | CST _v ->
            _menhir_run36 _menhir_env (Obj.magic _menhir_stack) MenhirState68 _v
        | IDENT _v ->
            _menhir_run35 _menhir_env (Obj.magic _menhir_stack) MenhirState68 _v
        | LPAR ->
            _menhir_run33 _menhir_env (Obj.magic _menhir_stack) MenhirState68
        | AND | COMMA | ELSE | EOF | EQ | GE | GT | IN | LE | LSL | LSR | LT | MINUS | MOD | NEQ | OR | PLUS | RBRACE | RPAR | SEMI | SLASH | STAR | THEN ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let (((_menhir_stack, _menhir_s, (e1 : (Mml.expression))), _), _, (e2 : (Mml.expression))) = _menhir_stack in
            let _10 = () in
            let _v : (Mml.expression) = let op =
              let _1 = _10 in
              
# 107 "mmlparser.mly"
      ( Lsr )
# 1003 "mmlparser.ml"
              
            in
            
# 80 "mmlparser.mly"
                                       ( Binop(op, e1, e2) )
# 1009 "mmlparser.ml"
             in
            _menhir_goto_expression _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState68)
    | MenhirState69 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | BOOL _v ->
            _menhir_run37 _menhir_env (Obj.magic _menhir_stack) MenhirState70 _v
        | CST _v ->
            _menhir_run36 _menhir_env (Obj.magic _menhir_stack) MenhirState70 _v
        | IDENT _v ->
            _menhir_run35 _menhir_env (Obj.magic _menhir_stack) MenhirState70 _v
        | LPAR ->
            _menhir_run33 _menhir_env (Obj.magic _menhir_stack) MenhirState70
        | AND | COMMA | ELSE | EOF | EQ | GE | GT | IN | LE | LSL | LSR | LT | MINUS | MOD | NEQ | OR | PLUS | RBRACE | RPAR | SEMI | SLASH | STAR | THEN ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let (((_menhir_stack, _menhir_s, (e1 : (Mml.expression))), _), _, (e2 : (Mml.expression))) = _menhir_stack in
            let _10 = () in
            let _v : (Mml.expression) = let op =
              let _1 = _10 in
              
# 106 "mmlparser.mly"
      ( Lsl )
# 1038 "mmlparser.ml"
              
            in
            
# 80 "mmlparser.mly"
                                       ( Binop(op, e1, e2) )
# 1044 "mmlparser.ml"
             in
            _menhir_goto_expression _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState70)
    | MenhirState71 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | BOOL _v ->
            _menhir_run37 _menhir_env (Obj.magic _menhir_stack) MenhirState72 _v
        | CST _v ->
            _menhir_run36 _menhir_env (Obj.magic _menhir_stack) MenhirState72 _v
        | IDENT _v ->
            _menhir_run35 _menhir_env (Obj.magic _menhir_stack) MenhirState72 _v
        | LPAR ->
            _menhir_run33 _menhir_env (Obj.magic _menhir_stack) MenhirState72
        | LSL ->
            _menhir_run69 _menhir_env (Obj.magic _menhir_stack) MenhirState72
        | LSR ->
            _menhir_run67 _menhir_env (Obj.magic _menhir_stack) MenhirState72
        | AND | COMMA | ELSE | EOF | EQ | GE | GT | IN | LE | LT | MINUS | MOD | NEQ | OR | PLUS | RBRACE | RPAR | SEMI | SLASH | STAR | THEN ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let (((_menhir_stack, _menhir_s, (e1 : (Mml.expression))), _), _, (e2 : (Mml.expression))) = _menhir_stack in
            let _10 = () in
            let _v : (Mml.expression) = let op =
              let _1 = _10 in
              
# 104 "mmlparser.mly"
        ( Div )
# 1077 "mmlparser.ml"
              
            in
            
# 80 "mmlparser.mly"
                                       ( Binop(op, e1, e2) )
# 1083 "mmlparser.ml"
             in
            _menhir_goto_expression _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState72)
    | MenhirState73 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | BOOL _v ->
            _menhir_run37 _menhir_env (Obj.magic _menhir_stack) MenhirState74 _v
        | CST _v ->
            _menhir_run36 _menhir_env (Obj.magic _menhir_stack) MenhirState74 _v
        | IDENT _v ->
            _menhir_run35 _menhir_env (Obj.magic _menhir_stack) MenhirState74 _v
        | LPAR ->
            _menhir_run33 _menhir_env (Obj.magic _menhir_stack) MenhirState74
        | LSL ->
            _menhir_run69 _menhir_env (Obj.magic _menhir_stack) MenhirState74
        | LSR ->
            _menhir_run67 _menhir_env (Obj.magic _menhir_stack) MenhirState74
        | MOD ->
            _menhir_run75 _menhir_env (Obj.magic _menhir_stack) MenhirState74
        | SLASH ->
            _menhir_run71 _menhir_env (Obj.magic _menhir_stack) MenhirState74
        | STAR ->
            _menhir_run65 _menhir_env (Obj.magic _menhir_stack) MenhirState74
        | AND | COMMA | ELSE | EOF | EQ | GE | GT | IN | LE | LT | MINUS | NEQ | OR | PLUS | RBRACE | RPAR | SEMI | THEN ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let (((_menhir_stack, _menhir_s, (e1 : (Mml.expression))), _), _, (e2 : (Mml.expression))) = _menhir_stack in
            let _10 = () in
            let _v : (Mml.expression) = let op =
              let _1 = _10 in
              
# 101 "mmlparser.mly"
       ( Add )
# 1122 "mmlparser.ml"
              
            in
            
# 80 "mmlparser.mly"
                                       ( Binop(op, e1, e2) )
# 1128 "mmlparser.ml"
             in
            _menhir_goto_expression _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState74)
    | MenhirState75 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | BOOL _v ->
            _menhir_run37 _menhir_env (Obj.magic _menhir_stack) MenhirState76 _v
        | CST _v ->
            _menhir_run36 _menhir_env (Obj.magic _menhir_stack) MenhirState76 _v
        | IDENT _v ->
            _menhir_run35 _menhir_env (Obj.magic _menhir_stack) MenhirState76 _v
        | LPAR ->
            _menhir_run33 _menhir_env (Obj.magic _menhir_stack) MenhirState76
        | LSL ->
            _menhir_run69 _menhir_env (Obj.magic _menhir_stack) MenhirState76
        | LSR ->
            _menhir_run67 _menhir_env (Obj.magic _menhir_stack) MenhirState76
        | AND | COMMA | ELSE | EOF | EQ | GE | GT | IN | LE | LT | MINUS | MOD | NEQ | OR | PLUS | RBRACE | RPAR | SEMI | SLASH | STAR | THEN ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let (((_menhir_stack, _menhir_s, (e1 : (Mml.expression))), _), _, (e2 : (Mml.expression))) = _menhir_stack in
            let _10 = () in
            let _v : (Mml.expression) = let op =
              let _1 = _10 in
              
# 105 "mmlparser.mly"
      ( Rem )
# 1161 "mmlparser.ml"
              
            in
            
# 80 "mmlparser.mly"
                                       ( Binop(op, e1, e2) )
# 1167 "mmlparser.ml"
             in
            _menhir_goto_expression _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState76)
    | MenhirState77 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | BOOL _v ->
            _menhir_run37 _menhir_env (Obj.magic _menhir_stack) MenhirState78 _v
        | CST _v ->
            _menhir_run36 _menhir_env (Obj.magic _menhir_stack) MenhirState78 _v
        | EQ ->
            _menhir_run91 _menhir_env (Obj.magic _menhir_stack) MenhirState78
        | GE ->
            _menhir_run89 _menhir_env (Obj.magic _menhir_stack) MenhirState78
        | GT ->
            _menhir_run87 _menhir_env (Obj.magic _menhir_stack) MenhirState78
        | IDENT _v ->
            _menhir_run35 _menhir_env (Obj.magic _menhir_stack) MenhirState78 _v
        | LE ->
            _menhir_run85 _menhir_env (Obj.magic _menhir_stack) MenhirState78
        | LPAR ->
            _menhir_run33 _menhir_env (Obj.magic _menhir_stack) MenhirState78
        | LSL ->
            _menhir_run69 _menhir_env (Obj.magic _menhir_stack) MenhirState78
        | LSR ->
            _menhir_run67 _menhir_env (Obj.magic _menhir_stack) MenhirState78
        | LT ->
            _menhir_run83 _menhir_env (Obj.magic _menhir_stack) MenhirState78
        | MINUS ->
            _menhir_run81 _menhir_env (Obj.magic _menhir_stack) MenhirState78
        | MOD ->
            _menhir_run75 _menhir_env (Obj.magic _menhir_stack) MenhirState78
        | NEQ ->
            _menhir_run79 _menhir_env (Obj.magic _menhir_stack) MenhirState78
        | PLUS ->
            _menhir_run73 _menhir_env (Obj.magic _menhir_stack) MenhirState78
        | SLASH ->
            _menhir_run71 _menhir_env (Obj.magic _menhir_stack) MenhirState78
        | STAR ->
            _menhir_run65 _menhir_env (Obj.magic _menhir_stack) MenhirState78
        | AND | COMMA | ELSE | EOF | IN | OR | RBRACE | RPAR | SEMI | THEN ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let (((_menhir_stack, _menhir_s, (e1 : (Mml.expression))), _), _, (e2 : (Mml.expression))) = _menhir_stack in
            let _10 = () in
            let _v : (Mml.expression) = let op =
              let _1 = _10 in
              
# 115 "mmlparser.mly"
     ( Or )
# 1222 "mmlparser.ml"
              
            in
            
# 80 "mmlparser.mly"
                                       ( Binop(op, e1, e2) )
# 1228 "mmlparser.ml"
             in
            _menhir_goto_expression _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState78)
    | MenhirState79 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | BOOL _v ->
            _menhir_run37 _menhir_env (Obj.magic _menhir_stack) MenhirState80 _v
        | CST _v ->
            _menhir_run36 _menhir_env (Obj.magic _menhir_stack) MenhirState80 _v
        | IDENT _v ->
            _menhir_run35 _menhir_env (Obj.magic _menhir_stack) MenhirState80 _v
        | LPAR ->
            _menhir_run33 _menhir_env (Obj.magic _menhir_stack) MenhirState80
        | LSL ->
            _menhir_run69 _menhir_env (Obj.magic _menhir_stack) MenhirState80
        | LSR ->
            _menhir_run67 _menhir_env (Obj.magic _menhir_stack) MenhirState80
        | MINUS ->
            _menhir_run81 _menhir_env (Obj.magic _menhir_stack) MenhirState80
        | MOD ->
            _menhir_run75 _menhir_env (Obj.magic _menhir_stack) MenhirState80
        | PLUS ->
            _menhir_run73 _menhir_env (Obj.magic _menhir_stack) MenhirState80
        | SLASH ->
            _menhir_run71 _menhir_env (Obj.magic _menhir_stack) MenhirState80
        | STAR ->
            _menhir_run65 _menhir_env (Obj.magic _menhir_stack) MenhirState80
        | AND | COMMA | ELSE | EOF | EQ | GE | GT | IN | LE | LT | NEQ | OR | RBRACE | RPAR | SEMI | THEN ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let (((_menhir_stack, _menhir_s, (e1 : (Mml.expression))), _), _, (e2 : (Mml.expression))) = _menhir_stack in
            let _10 = () in
            let _v : (Mml.expression) = let op =
              let _1 = _10 in
              
# 109 "mmlparser.mly"
      ( Neq )
# 1271 "mmlparser.ml"
              
            in
            
# 80 "mmlparser.mly"
                                       ( Binop(op, e1, e2) )
# 1277 "mmlparser.ml"
             in
            _menhir_goto_expression _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState80)
    | MenhirState81 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | BOOL _v ->
            _menhir_run37 _menhir_env (Obj.magic _menhir_stack) MenhirState82 _v
        | CST _v ->
            _menhir_run36 _menhir_env (Obj.magic _menhir_stack) MenhirState82 _v
        | IDENT _v ->
            _menhir_run35 _menhir_env (Obj.magic _menhir_stack) MenhirState82 _v
        | LPAR ->
            _menhir_run33 _menhir_env (Obj.magic _menhir_stack) MenhirState82
        | LSL ->
            _menhir_run69 _menhir_env (Obj.magic _menhir_stack) MenhirState82
        | LSR ->
            _menhir_run67 _menhir_env (Obj.magic _menhir_stack) MenhirState82
        | MOD ->
            _menhir_run75 _menhir_env (Obj.magic _menhir_stack) MenhirState82
        | SLASH ->
            _menhir_run71 _menhir_env (Obj.magic _menhir_stack) MenhirState82
        | STAR ->
            _menhir_run65 _menhir_env (Obj.magic _menhir_stack) MenhirState82
        | AND | COMMA | ELSE | EOF | EQ | GE | GT | IN | LE | LT | MINUS | NEQ | OR | PLUS | RBRACE | RPAR | SEMI | THEN ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let (((_menhir_stack, _menhir_s, (e1 : (Mml.expression))), _), _, (e2 : (Mml.expression))) = _menhir_stack in
            let _10 = () in
            let _v : (Mml.expression) = let op =
              let _1 = _10 in
              
# 102 "mmlparser.mly"
        ( Sub )
# 1316 "mmlparser.ml"
              
            in
            
# 80 "mmlparser.mly"
                                       ( Binop(op, e1, e2) )
# 1322 "mmlparser.ml"
             in
            _menhir_goto_expression _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState82)
    | MenhirState83 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | BOOL _v ->
            _menhir_run37 _menhir_env (Obj.magic _menhir_stack) MenhirState84 _v
        | CST _v ->
            _menhir_run36 _menhir_env (Obj.magic _menhir_stack) MenhirState84 _v
        | IDENT _v ->
            _menhir_run35 _menhir_env (Obj.magic _menhir_stack) MenhirState84 _v
        | LPAR ->
            _menhir_run33 _menhir_env (Obj.magic _menhir_stack) MenhirState84
        | LSL ->
            _menhir_run69 _menhir_env (Obj.magic _menhir_stack) MenhirState84
        | LSR ->
            _menhir_run67 _menhir_env (Obj.magic _menhir_stack) MenhirState84
        | MINUS ->
            _menhir_run81 _menhir_env (Obj.magic _menhir_stack) MenhirState84
        | MOD ->
            _menhir_run75 _menhir_env (Obj.magic _menhir_stack) MenhirState84
        | PLUS ->
            _menhir_run73 _menhir_env (Obj.magic _menhir_stack) MenhirState84
        | SLASH ->
            _menhir_run71 _menhir_env (Obj.magic _menhir_stack) MenhirState84
        | STAR ->
            _menhir_run65 _menhir_env (Obj.magic _menhir_stack) MenhirState84
        | AND | COMMA | ELSE | EOF | EQ | GE | GT | IN | LE | LT | NEQ | OR | RBRACE | RPAR | SEMI | THEN ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let (((_menhir_stack, _menhir_s, (e1 : (Mml.expression))), _), _, (e2 : (Mml.expression))) = _menhir_stack in
            let _10 = () in
            let _v : (Mml.expression) = let op =
              let _1 = _10 in
              
# 110 "mmlparser.mly"
     ( Lt )
# 1365 "mmlparser.ml"
              
            in
            
# 80 "mmlparser.mly"
                                       ( Binop(op, e1, e2) )
# 1371 "mmlparser.ml"
             in
            _menhir_goto_expression _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState84)
    | MenhirState85 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | BOOL _v ->
            _menhir_run37 _menhir_env (Obj.magic _menhir_stack) MenhirState86 _v
        | CST _v ->
            _menhir_run36 _menhir_env (Obj.magic _menhir_stack) MenhirState86 _v
        | IDENT _v ->
            _menhir_run35 _menhir_env (Obj.magic _menhir_stack) MenhirState86 _v
        | LPAR ->
            _menhir_run33 _menhir_env (Obj.magic _menhir_stack) MenhirState86
        | LSL ->
            _menhir_run69 _menhir_env (Obj.magic _menhir_stack) MenhirState86
        | LSR ->
            _menhir_run67 _menhir_env (Obj.magic _menhir_stack) MenhirState86
        | MINUS ->
            _menhir_run81 _menhir_env (Obj.magic _menhir_stack) MenhirState86
        | MOD ->
            _menhir_run75 _menhir_env (Obj.magic _menhir_stack) MenhirState86
        | PLUS ->
            _menhir_run73 _menhir_env (Obj.magic _menhir_stack) MenhirState86
        | SLASH ->
            _menhir_run71 _menhir_env (Obj.magic _menhir_stack) MenhirState86
        | STAR ->
            _menhir_run65 _menhir_env (Obj.magic _menhir_stack) MenhirState86
        | AND | COMMA | ELSE | EOF | EQ | GE | GT | IN | LE | LT | NEQ | OR | RBRACE | RPAR | SEMI | THEN ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let (((_menhir_stack, _menhir_s, (e1 : (Mml.expression))), _), _, (e2 : (Mml.expression))) = _menhir_stack in
            let _10 = () in
            let _v : (Mml.expression) = let op =
              let _1 = _10 in
              
# 111 "mmlparser.mly"
     ( Le )
# 1414 "mmlparser.ml"
              
            in
            
# 80 "mmlparser.mly"
                                       ( Binop(op, e1, e2) )
# 1420 "mmlparser.ml"
             in
            _menhir_goto_expression _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState86)
    | MenhirState87 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | BOOL _v ->
            _menhir_run37 _menhir_env (Obj.magic _menhir_stack) MenhirState88 _v
        | CST _v ->
            _menhir_run36 _menhir_env (Obj.magic _menhir_stack) MenhirState88 _v
        | IDENT _v ->
            _menhir_run35 _menhir_env (Obj.magic _menhir_stack) MenhirState88 _v
        | LPAR ->
            _menhir_run33 _menhir_env (Obj.magic _menhir_stack) MenhirState88
        | LSL ->
            _menhir_run69 _menhir_env (Obj.magic _menhir_stack) MenhirState88
        | LSR ->
            _menhir_run67 _menhir_env (Obj.magic _menhir_stack) MenhirState88
        | MINUS ->
            _menhir_run81 _menhir_env (Obj.magic _menhir_stack) MenhirState88
        | MOD ->
            _menhir_run75 _menhir_env (Obj.magic _menhir_stack) MenhirState88
        | PLUS ->
            _menhir_run73 _menhir_env (Obj.magic _menhir_stack) MenhirState88
        | SLASH ->
            _menhir_run71 _menhir_env (Obj.magic _menhir_stack) MenhirState88
        | STAR ->
            _menhir_run65 _menhir_env (Obj.magic _menhir_stack) MenhirState88
        | AND | COMMA | ELSE | EOF | EQ | GE | GT | IN | LE | LT | NEQ | OR | RBRACE | RPAR | SEMI | THEN ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let (((_menhir_stack, _menhir_s, (e1 : (Mml.expression))), _), _, (e2 : (Mml.expression))) = _menhir_stack in
            let _10 = () in
            let _v : (Mml.expression) = let op =
              let _1 = _10 in
              
# 112 "mmlparser.mly"
     ( Gt )
# 1463 "mmlparser.ml"
              
            in
            
# 80 "mmlparser.mly"
                                       ( Binop(op, e1, e2) )
# 1469 "mmlparser.ml"
             in
            _menhir_goto_expression _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState88)
    | MenhirState89 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | BOOL _v ->
            _menhir_run37 _menhir_env (Obj.magic _menhir_stack) MenhirState90 _v
        | CST _v ->
            _menhir_run36 _menhir_env (Obj.magic _menhir_stack) MenhirState90 _v
        | IDENT _v ->
            _menhir_run35 _menhir_env (Obj.magic _menhir_stack) MenhirState90 _v
        | LPAR ->
            _menhir_run33 _menhir_env (Obj.magic _menhir_stack) MenhirState90
        | LSL ->
            _menhir_run69 _menhir_env (Obj.magic _menhir_stack) MenhirState90
        | LSR ->
            _menhir_run67 _menhir_env (Obj.magic _menhir_stack) MenhirState90
        | MINUS ->
            _menhir_run81 _menhir_env (Obj.magic _menhir_stack) MenhirState90
        | MOD ->
            _menhir_run75 _menhir_env (Obj.magic _menhir_stack) MenhirState90
        | PLUS ->
            _menhir_run73 _menhir_env (Obj.magic _menhir_stack) MenhirState90
        | SLASH ->
            _menhir_run71 _menhir_env (Obj.magic _menhir_stack) MenhirState90
        | STAR ->
            _menhir_run65 _menhir_env (Obj.magic _menhir_stack) MenhirState90
        | AND | COMMA | ELSE | EOF | EQ | GE | GT | IN | LE | LT | NEQ | OR | RBRACE | RPAR | SEMI | THEN ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let (((_menhir_stack, _menhir_s, (e1 : (Mml.expression))), _), _, (e2 : (Mml.expression))) = _menhir_stack in
            let _10 = () in
            let _v : (Mml.expression) = let op =
              let _1 = _10 in
              
# 113 "mmlparser.mly"
     ( Ge )
# 1512 "mmlparser.ml"
              
            in
            
# 80 "mmlparser.mly"
                                       ( Binop(op, e1, e2) )
# 1518 "mmlparser.ml"
             in
            _menhir_goto_expression _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState90)
    | MenhirState91 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | BOOL _v ->
            _menhir_run37 _menhir_env (Obj.magic _menhir_stack) MenhirState92 _v
        | CST _v ->
            _menhir_run36 _menhir_env (Obj.magic _menhir_stack) MenhirState92 _v
        | IDENT _v ->
            _menhir_run35 _menhir_env (Obj.magic _menhir_stack) MenhirState92 _v
        | LPAR ->
            _menhir_run33 _menhir_env (Obj.magic _menhir_stack) MenhirState92
        | LSL ->
            _menhir_run69 _menhir_env (Obj.magic _menhir_stack) MenhirState92
        | LSR ->
            _menhir_run67 _menhir_env (Obj.magic _menhir_stack) MenhirState92
        | MINUS ->
            _menhir_run81 _menhir_env (Obj.magic _menhir_stack) MenhirState92
        | MOD ->
            _menhir_run75 _menhir_env (Obj.magic _menhir_stack) MenhirState92
        | PLUS ->
            _menhir_run73 _menhir_env (Obj.magic _menhir_stack) MenhirState92
        | SLASH ->
            _menhir_run71 _menhir_env (Obj.magic _menhir_stack) MenhirState92
        | STAR ->
            _menhir_run65 _menhir_env (Obj.magic _menhir_stack) MenhirState92
        | AND | COMMA | ELSE | EOF | EQ | GE | GT | IN | LE | LT | NEQ | OR | RBRACE | RPAR | SEMI | THEN ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let (((_menhir_stack, _menhir_s, (e1 : (Mml.expression))), _), _, (e2 : (Mml.expression))) = _menhir_stack in
            let _10 = () in
            let _v : (Mml.expression) = let op =
              let _1 = _10 in
              
# 108 "mmlparser.mly"
     ( Eq )
# 1561 "mmlparser.ml"
              
            in
            
# 80 "mmlparser.mly"
                                       ( Binop(op, e1, e2) )
# 1567 "mmlparser.ml"
             in
            _menhir_goto_expression _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState92)
    | MenhirState93 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | BOOL _v ->
            _menhir_run37 _menhir_env (Obj.magic _menhir_stack) MenhirState94 _v
        | CST _v ->
            _menhir_run36 _menhir_env (Obj.magic _menhir_stack) MenhirState94 _v
        | EQ ->
            _menhir_run91 _menhir_env (Obj.magic _menhir_stack) MenhirState94
        | GE ->
            _menhir_run89 _menhir_env (Obj.magic _menhir_stack) MenhirState94
        | GT ->
            _menhir_run87 _menhir_env (Obj.magic _menhir_stack) MenhirState94
        | IDENT _v ->
            _menhir_run35 _menhir_env (Obj.magic _menhir_stack) MenhirState94 _v
        | LE ->
            _menhir_run85 _menhir_env (Obj.magic _menhir_stack) MenhirState94
        | LPAR ->
            _menhir_run33 _menhir_env (Obj.magic _menhir_stack) MenhirState94
        | LSL ->
            _menhir_run69 _menhir_env (Obj.magic _menhir_stack) MenhirState94
        | LSR ->
            _menhir_run67 _menhir_env (Obj.magic _menhir_stack) MenhirState94
        | LT ->
            _menhir_run83 _menhir_env (Obj.magic _menhir_stack) MenhirState94
        | MINUS ->
            _menhir_run81 _menhir_env (Obj.magic _menhir_stack) MenhirState94
        | MOD ->
            _menhir_run75 _menhir_env (Obj.magic _menhir_stack) MenhirState94
        | NEQ ->
            _menhir_run79 _menhir_env (Obj.magic _menhir_stack) MenhirState94
        | PLUS ->
            _menhir_run73 _menhir_env (Obj.magic _menhir_stack) MenhirState94
        | SLASH ->
            _menhir_run71 _menhir_env (Obj.magic _menhir_stack) MenhirState94
        | STAR ->
            _menhir_run65 _menhir_env (Obj.magic _menhir_stack) MenhirState94
        | AND | COMMA | ELSE | EOF | IN | OR | RBRACE | RPAR | SEMI | THEN ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let (((_menhir_stack, _menhir_s, (e1 : (Mml.expression))), _), _, (e2 : (Mml.expression))) = _menhir_stack in
            let _10 = () in
            let _v : (Mml.expression) = let op =
              let _1 = _10 in
              
# 114 "mmlparser.mly"
      ( And )
# 1622 "mmlparser.ml"
              
            in
            
# 80 "mmlparser.mly"
                                       ( Binop(op, e1, e2) )
# 1628 "mmlparser.ml"
             in
            _menhir_goto_expression _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState94)
    | MenhirState54 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | AND ->
            _menhir_run93 _menhir_env (Obj.magic _menhir_stack) MenhirState95
        | BOOL _v ->
            _menhir_run37 _menhir_env (Obj.magic _menhir_stack) MenhirState95 _v
        | CST _v ->
            _menhir_run36 _menhir_env (Obj.magic _menhir_stack) MenhirState95 _v
        | EQ ->
            _menhir_run91 _menhir_env (Obj.magic _menhir_stack) MenhirState95
        | GE ->
            _menhir_run89 _menhir_env (Obj.magic _menhir_stack) MenhirState95
        | GT ->
            _menhir_run87 _menhir_env (Obj.magic _menhir_stack) MenhirState95
        | IDENT _v ->
            _menhir_run35 _menhir_env (Obj.magic _menhir_stack) MenhirState95 _v
        | LE ->
            _menhir_run85 _menhir_env (Obj.magic _menhir_stack) MenhirState95
        | LPAR ->
            _menhir_run33 _menhir_env (Obj.magic _menhir_stack) MenhirState95
        | LSL ->
            _menhir_run69 _menhir_env (Obj.magic _menhir_stack) MenhirState95
        | LSR ->
            _menhir_run67 _menhir_env (Obj.magic _menhir_stack) MenhirState95
        | LT ->
            _menhir_run83 _menhir_env (Obj.magic _menhir_stack) MenhirState95
        | MINUS ->
            _menhir_run81 _menhir_env (Obj.magic _menhir_stack) MenhirState95
        | MOD ->
            _menhir_run75 _menhir_env (Obj.magic _menhir_stack) MenhirState95
        | NEQ ->
            _menhir_run79 _menhir_env (Obj.magic _menhir_stack) MenhirState95
        | OR ->
            _menhir_run77 _menhir_env (Obj.magic _menhir_stack) MenhirState95
        | PLUS ->
            _menhir_run73 _menhir_env (Obj.magic _menhir_stack) MenhirState95
        | SLASH ->
            _menhir_run71 _menhir_env (Obj.magic _menhir_stack) MenhirState95
        | STAR ->
            _menhir_run65 _menhir_env (Obj.magic _menhir_stack) MenhirState95
        | THEN ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_s = MenhirState95 in
            let _menhir_stack = (_menhir_stack, _menhir_s) in
            let _menhir_env = _menhir_discard _menhir_env in
            let _tok = _menhir_env._menhir_token in
            (match _tok with
            | BOOL _v ->
                _menhir_run37 _menhir_env (Obj.magic _menhir_stack) MenhirState96 _v
            | CST _v ->
                _menhir_run36 _menhir_env (Obj.magic _menhir_stack) MenhirState96 _v
            | FST ->
                _menhir_run60 _menhir_env (Obj.magic _menhir_stack) MenhirState96
            | FUN ->
                _menhir_run55 _menhir_env (Obj.magic _menhir_stack) MenhirState96
            | IDENT _v ->
                _menhir_run35 _menhir_env (Obj.magic _menhir_stack) MenhirState96 _v
            | IF ->
                _menhir_run54 _menhir_env (Obj.magic _menhir_stack) MenhirState96
            | LBRACE ->
                _menhir_run51 _menhir_env (Obj.magic _menhir_stack) MenhirState96
            | LET ->
                _menhir_run42 _menhir_env (Obj.magic _menhir_stack) MenhirState96
            | LPAR ->
                _menhir_run41 _menhir_env (Obj.magic _menhir_stack) MenhirState96
            | MINUS ->
                _menhir_run34 _menhir_env (Obj.magic _menhir_stack) MenhirState96
            | NOT ->
                _menhir_run32 _menhir_env (Obj.magic _menhir_stack) MenhirState96
            | SND ->
                _menhir_run31 _menhir_env (Obj.magic _menhir_stack) MenhirState96
            | _ ->
                assert (not _menhir_env._menhir_error);
                _menhir_env._menhir_error <- true;
                _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState96)
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState95)
    | MenhirState96 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | AND ->
            _menhir_run93 _menhir_env (Obj.magic _menhir_stack) MenhirState97
        | BOOL _v ->
            _menhir_run37 _menhir_env (Obj.magic _menhir_stack) MenhirState97 _v
        | CST _v ->
            _menhir_run36 _menhir_env (Obj.magic _menhir_stack) MenhirState97 _v
        | ELSE ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_s = MenhirState97 in
            let _menhir_stack = (_menhir_stack, _menhir_s) in
            let _menhir_env = _menhir_discard _menhir_env in
            let _tok = _menhir_env._menhir_token in
            (match _tok with
            | BOOL _v ->
                _menhir_run37 _menhir_env (Obj.magic _menhir_stack) MenhirState98 _v
            | CST _v ->
                _menhir_run36 _menhir_env (Obj.magic _menhir_stack) MenhirState98 _v
            | FST ->
                _menhir_run60 _menhir_env (Obj.magic _menhir_stack) MenhirState98
            | FUN ->
                _menhir_run55 _menhir_env (Obj.magic _menhir_stack) MenhirState98
            | IDENT _v ->
                _menhir_run35 _menhir_env (Obj.magic _menhir_stack) MenhirState98 _v
            | IF ->
                _menhir_run54 _menhir_env (Obj.magic _menhir_stack) MenhirState98
            | LBRACE ->
                _menhir_run51 _menhir_env (Obj.magic _menhir_stack) MenhirState98
            | LET ->
                _menhir_run42 _menhir_env (Obj.magic _menhir_stack) MenhirState98
            | LPAR ->
                _menhir_run41 _menhir_env (Obj.magic _menhir_stack) MenhirState98
            | MINUS ->
                _menhir_run34 _menhir_env (Obj.magic _menhir_stack) MenhirState98
            | NOT ->
                _menhir_run32 _menhir_env (Obj.magic _menhir_stack) MenhirState98
            | SND ->
                _menhir_run31 _menhir_env (Obj.magic _menhir_stack) MenhirState98
            | _ ->
                assert (not _menhir_env._menhir_error);
                _menhir_env._menhir_error <- true;
                _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState98)
        | EQ ->
            _menhir_run91 _menhir_env (Obj.magic _menhir_stack) MenhirState97
        | GE ->
            _menhir_run89 _menhir_env (Obj.magic _menhir_stack) MenhirState97
        | GT ->
            _menhir_run87 _menhir_env (Obj.magic _menhir_stack) MenhirState97
        | IDENT _v ->
            _menhir_run35 _menhir_env (Obj.magic _menhir_stack) MenhirState97 _v
        | LE ->
            _menhir_run85 _menhir_env (Obj.magic _menhir_stack) MenhirState97
        | LPAR ->
            _menhir_run33 _menhir_env (Obj.magic _menhir_stack) MenhirState97
        | LSL ->
            _menhir_run69 _menhir_env (Obj.magic _menhir_stack) MenhirState97
        | LSR ->
            _menhir_run67 _menhir_env (Obj.magic _menhir_stack) MenhirState97
        | LT ->
            _menhir_run83 _menhir_env (Obj.magic _menhir_stack) MenhirState97
        | MINUS ->
            _menhir_run81 _menhir_env (Obj.magic _menhir_stack) MenhirState97
        | MOD ->
            _menhir_run75 _menhir_env (Obj.magic _menhir_stack) MenhirState97
        | NEQ ->
            _menhir_run79 _menhir_env (Obj.magic _menhir_stack) MenhirState97
        | OR ->
            _menhir_run77 _menhir_env (Obj.magic _menhir_stack) MenhirState97
        | PLUS ->
            _menhir_run73 _menhir_env (Obj.magic _menhir_stack) MenhirState97
        | SLASH ->
            _menhir_run71 _menhir_env (Obj.magic _menhir_stack) MenhirState97
        | STAR ->
            _menhir_run65 _menhir_env (Obj.magic _menhir_stack) MenhirState97
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState97)
    | MenhirState98 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | AND ->
            _menhir_run93 _menhir_env (Obj.magic _menhir_stack) MenhirState99
        | BOOL _v ->
            _menhir_run37 _menhir_env (Obj.magic _menhir_stack) MenhirState99 _v
        | CST _v ->
            _menhir_run36 _menhir_env (Obj.magic _menhir_stack) MenhirState99 _v
        | EQ ->
            _menhir_run91 _menhir_env (Obj.magic _menhir_stack) MenhirState99
        | GE ->
            _menhir_run89 _menhir_env (Obj.magic _menhir_stack) MenhirState99
        | GT ->
            _menhir_run87 _menhir_env (Obj.magic _menhir_stack) MenhirState99
        | IDENT _v ->
            _menhir_run35 _menhir_env (Obj.magic _menhir_stack) MenhirState99 _v
        | LE ->
            _menhir_run85 _menhir_env (Obj.magic _menhir_stack) MenhirState99
        | LPAR ->
            _menhir_run33 _menhir_env (Obj.magic _menhir_stack) MenhirState99
        | LSL ->
            _menhir_run69 _menhir_env (Obj.magic _menhir_stack) MenhirState99
        | LSR ->
            _menhir_run67 _menhir_env (Obj.magic _menhir_stack) MenhirState99
        | LT ->
            _menhir_run83 _menhir_env (Obj.magic _menhir_stack) MenhirState99
        | MINUS ->
            _menhir_run81 _menhir_env (Obj.magic _menhir_stack) MenhirState99
        | MOD ->
            _menhir_run75 _menhir_env (Obj.magic _menhir_stack) MenhirState99
        | NEQ ->
            _menhir_run79 _menhir_env (Obj.magic _menhir_stack) MenhirState99
        | OR ->
            _menhir_run77 _menhir_env (Obj.magic _menhir_stack) MenhirState99
        | PLUS ->
            _menhir_run73 _menhir_env (Obj.magic _menhir_stack) MenhirState99
        | SLASH ->
            _menhir_run71 _menhir_env (Obj.magic _menhir_stack) MenhirState99
        | STAR ->
            _menhir_run65 _menhir_env (Obj.magic _menhir_stack) MenhirState99
        | COMMA | ELSE | EOF | IN | RBRACE | RPAR | SEMI | THEN ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let ((((((_menhir_stack, _menhir_s), _, (e1 : (Mml.expression))), _), _, (e2 : (Mml.expression))), _), _, (e3 : (Mml.expression))) = _menhir_stack in
            let _5 = () in
            let _3 = () in
            let _1 = () in
            let _v : (Mml.expression) = 
# 87 "mmlparser.mly"
                                                         ( If(e1, e2, e3) )
# 1851 "mmlparser.ml"
             in
            _menhir_goto_expression _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState99)
    | MenhirState53 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | AND ->
            _menhir_run93 _menhir_env (Obj.magic _menhir_stack) MenhirState100
        | BOOL _v ->
            _menhir_run37 _menhir_env (Obj.magic _menhir_stack) MenhirState100 _v
        | CST _v ->
            _menhir_run36 _menhir_env (Obj.magic _menhir_stack) MenhirState100 _v
        | EQ ->
            _menhir_run91 _menhir_env (Obj.magic _menhir_stack) MenhirState100
        | GE ->
            _menhir_run89 _menhir_env (Obj.magic _menhir_stack) MenhirState100
        | GT ->
            _menhir_run87 _menhir_env (Obj.magic _menhir_stack) MenhirState100
        | IDENT _v ->
            _menhir_run35 _menhir_env (Obj.magic _menhir_stack) MenhirState100 _v
        | LE ->
            _menhir_run85 _menhir_env (Obj.magic _menhir_stack) MenhirState100
        | LPAR ->
            _menhir_run33 _menhir_env (Obj.magic _menhir_stack) MenhirState100
        | LSL ->
            _menhir_run69 _menhir_env (Obj.magic _menhir_stack) MenhirState100
        | LSR ->
            _menhir_run67 _menhir_env (Obj.magic _menhir_stack) MenhirState100
        | LT ->
            _menhir_run83 _menhir_env (Obj.magic _menhir_stack) MenhirState100
        | MINUS ->
            _menhir_run81 _menhir_env (Obj.magic _menhir_stack) MenhirState100
        | MOD ->
            _menhir_run75 _menhir_env (Obj.magic _menhir_stack) MenhirState100
        | NEQ ->
            _menhir_run79 _menhir_env (Obj.magic _menhir_stack) MenhirState100
        | OR ->
            _menhir_run77 _menhir_env (Obj.magic _menhir_stack) MenhirState100
        | PLUS ->
            _menhir_run73 _menhir_env (Obj.magic _menhir_stack) MenhirState100
        | SLASH ->
            _menhir_run71 _menhir_env (Obj.magic _menhir_stack) MenhirState100
        | STAR ->
            _menhir_run65 _menhir_env (Obj.magic _menhir_stack) MenhirState100
        | RBRACE | SEMI ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let ((_menhir_stack, _menhir_s, (x : (
# 16 "mmlparser.mly"
       (string)
# 1906 "mmlparser.ml"
            ))), _, (e : (Mml.expression))) = _menhir_stack in
            let _2 = () in
            let _v : (string * Mml.expression) = 
# 66 "mmlparser.mly"
                          ( (x, e) )
# 1912 "mmlparser.ml"
             in
            let _menhir_stack = (_menhir_stack, _menhir_s, _v) in
            let _menhir_stack = Obj.magic _menhir_stack in
            assert (not _menhir_env._menhir_error);
            let _tok = _menhir_env._menhir_token in
            (match _tok with
            | SEMI ->
                let _menhir_stack = Obj.magic _menhir_stack in
                let _menhir_env = _menhir_discard _menhir_env in
                let _tok = _menhir_env._menhir_token in
                (match _tok with
                | IDENT _v ->
                    _menhir_run52 _menhir_env (Obj.magic _menhir_stack) MenhirState105 _v
                | _ ->
                    assert (not _menhir_env._menhir_error);
                    _menhir_env._menhir_error <- true;
                    _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState105)
            | RBRACE ->
                let _menhir_stack = Obj.magic _menhir_stack in
                let (_menhir_stack, _menhir_s, (x : (string * Mml.expression))) = _menhir_stack in
                let _v : ((string * Mml.expression) list) = 
# 229 "/home/mohamedh/.opam/system/lib/menhir/standard.mly"
    ( [ x ] )
# 1936 "mmlparser.ml"
                 in
                _menhir_goto_separated_nonempty_list_SEMI_field_def_ _menhir_env _menhir_stack _menhir_s _v
            | _ ->
                assert (not _menhir_env._menhir_error);
                _menhir_env._menhir_error <- true;
                let _menhir_stack = Obj.magic _menhir_stack in
                let (_menhir_stack, _menhir_s, _) = _menhir_stack in
                _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState100)
    | MenhirState50 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | AND ->
            _menhir_run93 _menhir_env (Obj.magic _menhir_stack) MenhirState107
        | BOOL _v ->
            _menhir_run37 _menhir_env (Obj.magic _menhir_stack) MenhirState107 _v
        | CST _v ->
            _menhir_run36 _menhir_env (Obj.magic _menhir_stack) MenhirState107 _v
        | EQ ->
            _menhir_run91 _menhir_env (Obj.magic _menhir_stack) MenhirState107
        | GE ->
            _menhir_run89 _menhir_env (Obj.magic _menhir_stack) MenhirState107
        | GT ->
            _menhir_run87 _menhir_env (Obj.magic _menhir_stack) MenhirState107
        | IDENT _v ->
            _menhir_run35 _menhir_env (Obj.magic _menhir_stack) MenhirState107 _v
        | IN ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_s = MenhirState107 in
            let _menhir_stack = (_menhir_stack, _menhir_s) in
            let _menhir_env = _menhir_discard _menhir_env in
            let _tok = _menhir_env._menhir_token in
            (match _tok with
            | BOOL _v ->
                _menhir_run37 _menhir_env (Obj.magic _menhir_stack) MenhirState108 _v
            | CST _v ->
                _menhir_run36 _menhir_env (Obj.magic _menhir_stack) MenhirState108 _v
            | FST ->
                _menhir_run60 _menhir_env (Obj.magic _menhir_stack) MenhirState108
            | FUN ->
                _menhir_run55 _menhir_env (Obj.magic _menhir_stack) MenhirState108
            | IDENT _v ->
                _menhir_run35 _menhir_env (Obj.magic _menhir_stack) MenhirState108 _v
            | IF ->
                _menhir_run54 _menhir_env (Obj.magic _menhir_stack) MenhirState108
            | LBRACE ->
                _menhir_run51 _menhir_env (Obj.magic _menhir_stack) MenhirState108
            | LET ->
                _menhir_run42 _menhir_env (Obj.magic _menhir_stack) MenhirState108
            | LPAR ->
                _menhir_run41 _menhir_env (Obj.magic _menhir_stack) MenhirState108
            | MINUS ->
                _menhir_run34 _menhir_env (Obj.magic _menhir_stack) MenhirState108
            | NOT ->
                _menhir_run32 _menhir_env (Obj.magic _menhir_stack) MenhirState108
            | SND ->
                _menhir_run31 _menhir_env (Obj.magic _menhir_stack) MenhirState108
            | _ ->
                assert (not _menhir_env._menhir_error);
                _menhir_env._menhir_error <- true;
                _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState108)
        | LE ->
            _menhir_run85 _menhir_env (Obj.magic _menhir_stack) MenhirState107
        | LPAR ->
            _menhir_run33 _menhir_env (Obj.magic _menhir_stack) MenhirState107
        | LSL ->
            _menhir_run69 _menhir_env (Obj.magic _menhir_stack) MenhirState107
        | LSR ->
            _menhir_run67 _menhir_env (Obj.magic _menhir_stack) MenhirState107
        | LT ->
            _menhir_run83 _menhir_env (Obj.magic _menhir_stack) MenhirState107
        | MINUS ->
            _menhir_run81 _menhir_env (Obj.magic _menhir_stack) MenhirState107
        | MOD ->
            _menhir_run75 _menhir_env (Obj.magic _menhir_stack) MenhirState107
        | NEQ ->
            _menhir_run79 _menhir_env (Obj.magic _menhir_stack) MenhirState107
        | OR ->
            _menhir_run77 _menhir_env (Obj.magic _menhir_stack) MenhirState107
        | PLUS ->
            _menhir_run73 _menhir_env (Obj.magic _menhir_stack) MenhirState107
        | SLASH ->
            _menhir_run71 _menhir_env (Obj.magic _menhir_stack) MenhirState107
        | STAR ->
            _menhir_run65 _menhir_env (Obj.magic _menhir_stack) MenhirState107
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState107)
    | MenhirState108 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | AND ->
            _menhir_run93 _menhir_env (Obj.magic _menhir_stack) MenhirState109
        | BOOL _v ->
            _menhir_run37 _menhir_env (Obj.magic _menhir_stack) MenhirState109 _v
        | CST _v ->
            _menhir_run36 _menhir_env (Obj.magic _menhir_stack) MenhirState109 _v
        | EQ ->
            _menhir_run91 _menhir_env (Obj.magic _menhir_stack) MenhirState109
        | GE ->
            _menhir_run89 _menhir_env (Obj.magic _menhir_stack) MenhirState109
        | GT ->
            _menhir_run87 _menhir_env (Obj.magic _menhir_stack) MenhirState109
        | IDENT _v ->
            _menhir_run35 _menhir_env (Obj.magic _menhir_stack) MenhirState109 _v
        | LE ->
            _menhir_run85 _menhir_env (Obj.magic _menhir_stack) MenhirState109
        | LPAR ->
            _menhir_run33 _menhir_env (Obj.magic _menhir_stack) MenhirState109
        | LSL ->
            _menhir_run69 _menhir_env (Obj.magic _menhir_stack) MenhirState109
        | LSR ->
            _menhir_run67 _menhir_env (Obj.magic _menhir_stack) MenhirState109
        | LT ->
            _menhir_run83 _menhir_env (Obj.magic _menhir_stack) MenhirState109
        | MINUS ->
            _menhir_run81 _menhir_env (Obj.magic _menhir_stack) MenhirState109
        | MOD ->
            _menhir_run75 _menhir_env (Obj.magic _menhir_stack) MenhirState109
        | NEQ ->
            _menhir_run79 _menhir_env (Obj.magic _menhir_stack) MenhirState109
        | OR ->
            _menhir_run77 _menhir_env (Obj.magic _menhir_stack) MenhirState109
        | PLUS ->
            _menhir_run73 _menhir_env (Obj.magic _menhir_stack) MenhirState109
        | SLASH ->
            _menhir_run71 _menhir_env (Obj.magic _menhir_stack) MenhirState109
        | STAR ->
            _menhir_run65 _menhir_env (Obj.magic _menhir_stack) MenhirState109
        | COMMA | ELSE | EOF | IN | RBRACE | RPAR | SEMI | THEN ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let (((((((_menhir_stack, _menhir_s), (f : (
# 16 "mmlparser.mly"
       (string)
# 2079 "mmlparser.ml"
            ))), _, (txs : ((string * Types.ty) list))), _, (alpha : (Types.ty))), _, (e1 : (Mml.expression))), _), _, (e2 : (Mml.expression))) = _menhir_stack in
            let _9 = () in
            let _7 = () in
            let _5 = () in
            let _2 = () in
            let _1 = () in
            let _v : (Mml.expression) = 
# 92 "mmlparser.mly"
    ( LetRec(f, mk_fun_type txs alpha, mk_fun txs e1, e2) )
# 2089 "mmlparser.ml"
             in
            _menhir_goto_expression _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState109)
    | MenhirState112 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | AND ->
            _menhir_run93 _menhir_env (Obj.magic _menhir_stack) MenhirState113
        | BOOL _v ->
            _menhir_run37 _menhir_env (Obj.magic _menhir_stack) MenhirState113 _v
        | CST _v ->
            _menhir_run36 _menhir_env (Obj.magic _menhir_stack) MenhirState113 _v
        | EQ ->
            _menhir_run91 _menhir_env (Obj.magic _menhir_stack) MenhirState113
        | GE ->
            _menhir_run89 _menhir_env (Obj.magic _menhir_stack) MenhirState113
        | GT ->
            _menhir_run87 _menhir_env (Obj.magic _menhir_stack) MenhirState113
        | IDENT _v ->
            _menhir_run35 _menhir_env (Obj.magic _menhir_stack) MenhirState113 _v
        | IN ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_s = MenhirState113 in
            let _menhir_stack = (_menhir_stack, _menhir_s) in
            let _menhir_env = _menhir_discard _menhir_env in
            let _tok = _menhir_env._menhir_token in
            (match _tok with
            | BOOL _v ->
                _menhir_run37 _menhir_env (Obj.magic _menhir_stack) MenhirState114 _v
            | CST _v ->
                _menhir_run36 _menhir_env (Obj.magic _menhir_stack) MenhirState114 _v
            | FST ->
                _menhir_run60 _menhir_env (Obj.magic _menhir_stack) MenhirState114
            | FUN ->
                _menhir_run55 _menhir_env (Obj.magic _menhir_stack) MenhirState114
            | IDENT _v ->
                _menhir_run35 _menhir_env (Obj.magic _menhir_stack) MenhirState114 _v
            | IF ->
                _menhir_run54 _menhir_env (Obj.magic _menhir_stack) MenhirState114
            | LBRACE ->
                _menhir_run51 _menhir_env (Obj.magic _menhir_stack) MenhirState114
            | LET ->
                _menhir_run42 _menhir_env (Obj.magic _menhir_stack) MenhirState114
            | LPAR ->
                _menhir_run41 _menhir_env (Obj.magic _menhir_stack) MenhirState114
            | MINUS ->
                _menhir_run34 _menhir_env (Obj.magic _menhir_stack) MenhirState114
            | NOT ->
                _menhir_run32 _menhir_env (Obj.magic _menhir_stack) MenhirState114
            | SND ->
                _menhir_run31 _menhir_env (Obj.magic _menhir_stack) MenhirState114
            | _ ->
                assert (not _menhir_env._menhir_error);
                _menhir_env._menhir_error <- true;
                _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState114)
        | LE ->
            _menhir_run85 _menhir_env (Obj.magic _menhir_stack) MenhirState113
        | LPAR ->
            _menhir_run33 _menhir_env (Obj.magic _menhir_stack) MenhirState113
        | LSL ->
            _menhir_run69 _menhir_env (Obj.magic _menhir_stack) MenhirState113
        | LSR ->
            _menhir_run67 _menhir_env (Obj.magic _menhir_stack) MenhirState113
        | LT ->
            _menhir_run83 _menhir_env (Obj.magic _menhir_stack) MenhirState113
        | MINUS ->
            _menhir_run81 _menhir_env (Obj.magic _menhir_stack) MenhirState113
        | MOD ->
            _menhir_run75 _menhir_env (Obj.magic _menhir_stack) MenhirState113
        | NEQ ->
            _menhir_run79 _menhir_env (Obj.magic _menhir_stack) MenhirState113
        | OR ->
            _menhir_run77 _menhir_env (Obj.magic _menhir_stack) MenhirState113
        | PLUS ->
            _menhir_run73 _menhir_env (Obj.magic _menhir_stack) MenhirState113
        | SLASH ->
            _menhir_run71 _menhir_env (Obj.magic _menhir_stack) MenhirState113
        | STAR ->
            _menhir_run65 _menhir_env (Obj.magic _menhir_stack) MenhirState113
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState113)
    | MenhirState114 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | AND ->
            _menhir_run93 _menhir_env (Obj.magic _menhir_stack) MenhirState115
        | BOOL _v ->
            _menhir_run37 _menhir_env (Obj.magic _menhir_stack) MenhirState115 _v
        | CST _v ->
            _menhir_run36 _menhir_env (Obj.magic _menhir_stack) MenhirState115 _v
        | EQ ->
            _menhir_run91 _menhir_env (Obj.magic _menhir_stack) MenhirState115
        | GE ->
            _menhir_run89 _menhir_env (Obj.magic _menhir_stack) MenhirState115
        | GT ->
            _menhir_run87 _menhir_env (Obj.magic _menhir_stack) MenhirState115
        | IDENT _v ->
            _menhir_run35 _menhir_env (Obj.magic _menhir_stack) MenhirState115 _v
        | LE ->
            _menhir_run85 _menhir_env (Obj.magic _menhir_stack) MenhirState115
        | LPAR ->
            _menhir_run33 _menhir_env (Obj.magic _menhir_stack) MenhirState115
        | LSL ->
            _menhir_run69 _menhir_env (Obj.magic _menhir_stack) MenhirState115
        | LSR ->
            _menhir_run67 _menhir_env (Obj.magic _menhir_stack) MenhirState115
        | LT ->
            _menhir_run83 _menhir_env (Obj.magic _menhir_stack) MenhirState115
        | MINUS ->
            _menhir_run81 _menhir_env (Obj.magic _menhir_stack) MenhirState115
        | MOD ->
            _menhir_run75 _menhir_env (Obj.magic _menhir_stack) MenhirState115
        | NEQ ->
            _menhir_run79 _menhir_env (Obj.magic _menhir_stack) MenhirState115
        | OR ->
            _menhir_run77 _menhir_env (Obj.magic _menhir_stack) MenhirState115
        | PLUS ->
            _menhir_run73 _menhir_env (Obj.magic _menhir_stack) MenhirState115
        | SLASH ->
            _menhir_run71 _menhir_env (Obj.magic _menhir_stack) MenhirState115
        | STAR ->
            _menhir_run65 _menhir_env (Obj.magic _menhir_stack) MenhirState115
        | COMMA | ELSE | EOF | IN | RBRACE | RPAR | SEMI | THEN ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let ((((((_menhir_stack, _menhir_s), (f : (
# 16 "mmlparser.mly"
       (string)
# 2226 "mmlparser.ml"
            ))), _, (txs : ((string * Types.ty) list))), _, (e1 : (Mml.expression))), _), _, (e2 : (Mml.expression))) = _menhir_stack in
            let _6 = () in
            let _4 = () in
            let _1 = () in
            let _v : (Mml.expression) = 
# 90 "mmlparser.mly"
    ( LetIn(f, mk_fun txs e1, e2) )
# 2234 "mmlparser.ml"
             in
            _menhir_goto_expression _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState115)
    | MenhirState41 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | AND ->
            _menhir_run93 _menhir_env (Obj.magic _menhir_stack) MenhirState116
        | BOOL _v ->
            _menhir_run37 _menhir_env (Obj.magic _menhir_stack) MenhirState116 _v
        | COMMA ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_s = MenhirState116 in
            let _menhir_stack = (_menhir_stack, _menhir_s) in
            let _menhir_env = _menhir_discard _menhir_env in
            let _tok = _menhir_env._menhir_token in
            (match _tok with
            | BOOL _v ->
                _menhir_run37 _menhir_env (Obj.magic _menhir_stack) MenhirState118 _v
            | CST _v ->
                _menhir_run36 _menhir_env (Obj.magic _menhir_stack) MenhirState118 _v
            | FST ->
                _menhir_run60 _menhir_env (Obj.magic _menhir_stack) MenhirState118
            | FUN ->
                _menhir_run55 _menhir_env (Obj.magic _menhir_stack) MenhirState118
            | IDENT _v ->
                _menhir_run35 _menhir_env (Obj.magic _menhir_stack) MenhirState118 _v
            | IF ->
                _menhir_run54 _menhir_env (Obj.magic _menhir_stack) MenhirState118
            | LBRACE ->
                _menhir_run51 _menhir_env (Obj.magic _menhir_stack) MenhirState118
            | LET ->
                _menhir_run42 _menhir_env (Obj.magic _menhir_stack) MenhirState118
            | LPAR ->
                _menhir_run41 _menhir_env (Obj.magic _menhir_stack) MenhirState118
            | MINUS ->
                _menhir_run34 _menhir_env (Obj.magic _menhir_stack) MenhirState118
            | NOT ->
                _menhir_run32 _menhir_env (Obj.magic _menhir_stack) MenhirState118
            | SND ->
                _menhir_run31 _menhir_env (Obj.magic _menhir_stack) MenhirState118
            | _ ->
                assert (not _menhir_env._menhir_error);
                _menhir_env._menhir_error <- true;
                _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState118)
        | CST _v ->
            _menhir_run36 _menhir_env (Obj.magic _menhir_stack) MenhirState116 _v
        | EQ ->
            _menhir_run91 _menhir_env (Obj.magic _menhir_stack) MenhirState116
        | GE ->
            _menhir_run89 _menhir_env (Obj.magic _menhir_stack) MenhirState116
        | GT ->
            _menhir_run87 _menhir_env (Obj.magic _menhir_stack) MenhirState116
        | IDENT _v ->
            _menhir_run35 _menhir_env (Obj.magic _menhir_stack) MenhirState116 _v
        | LE ->
            _menhir_run85 _menhir_env (Obj.magic _menhir_stack) MenhirState116
        | LPAR ->
            _menhir_run33 _menhir_env (Obj.magic _menhir_stack) MenhirState116
        | LSL ->
            _menhir_run69 _menhir_env (Obj.magic _menhir_stack) MenhirState116
        | LSR ->
            _menhir_run67 _menhir_env (Obj.magic _menhir_stack) MenhirState116
        | LT ->
            _menhir_run83 _menhir_env (Obj.magic _menhir_stack) MenhirState116
        | MINUS ->
            _menhir_run81 _menhir_env (Obj.magic _menhir_stack) MenhirState116
        | MOD ->
            _menhir_run75 _menhir_env (Obj.magic _menhir_stack) MenhirState116
        | NEQ ->
            _menhir_run79 _menhir_env (Obj.magic _menhir_stack) MenhirState116
        | OR ->
            _menhir_run77 _menhir_env (Obj.magic _menhir_stack) MenhirState116
        | PLUS ->
            _menhir_run73 _menhir_env (Obj.magic _menhir_stack) MenhirState116
        | RPAR ->
            _menhir_run117 _menhir_env (Obj.magic _menhir_stack) MenhirState116
        | SLASH ->
            _menhir_run71 _menhir_env (Obj.magic _menhir_stack) MenhirState116
        | STAR ->
            _menhir_run65 _menhir_env (Obj.magic _menhir_stack) MenhirState116
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState116)
    | MenhirState118 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | AND ->
            _menhir_run93 _menhir_env (Obj.magic _menhir_stack) MenhirState119
        | BOOL _v ->
            _menhir_run37 _menhir_env (Obj.magic _menhir_stack) MenhirState119 _v
        | CST _v ->
            _menhir_run36 _menhir_env (Obj.magic _menhir_stack) MenhirState119 _v
        | EQ ->
            _menhir_run91 _menhir_env (Obj.magic _menhir_stack) MenhirState119
        | GE ->
            _menhir_run89 _menhir_env (Obj.magic _menhir_stack) MenhirState119
        | GT ->
            _menhir_run87 _menhir_env (Obj.magic _menhir_stack) MenhirState119
        | IDENT _v ->
            _menhir_run35 _menhir_env (Obj.magic _menhir_stack) MenhirState119 _v
        | LE ->
            _menhir_run85 _menhir_env (Obj.magic _menhir_stack) MenhirState119
        | LPAR ->
            _menhir_run33 _menhir_env (Obj.magic _menhir_stack) MenhirState119
        | LSL ->
            _menhir_run69 _menhir_env (Obj.magic _menhir_stack) MenhirState119
        | LSR ->
            _menhir_run67 _menhir_env (Obj.magic _menhir_stack) MenhirState119
        | LT ->
            _menhir_run83 _menhir_env (Obj.magic _menhir_stack) MenhirState119
        | MINUS ->
            _menhir_run81 _menhir_env (Obj.magic _menhir_stack) MenhirState119
        | MOD ->
            _menhir_run75 _menhir_env (Obj.magic _menhir_stack) MenhirState119
        | NEQ ->
            _menhir_run79 _menhir_env (Obj.magic _menhir_stack) MenhirState119
        | OR ->
            _menhir_run77 _menhir_env (Obj.magic _menhir_stack) MenhirState119
        | PLUS ->
            _menhir_run73 _menhir_env (Obj.magic _menhir_stack) MenhirState119
        | RPAR ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_s = MenhirState119 in
            let _menhir_env = _menhir_discard _menhir_env in
            let _menhir_stack = Obj.magic _menhir_stack in
            let ((((_menhir_stack, _menhir_s), _, (e1 : (Mml.expression))), _), _, (e2 : (Mml.expression))) = _menhir_stack in
            let _5 = () in
            let _3 = () in
            let _1 = () in
            let _v : (Mml.expression) = 
# 81 "mmlparser.mly"
                                              ( Pair(e1, e2) )
# 2376 "mmlparser.ml"
             in
            _menhir_goto_expression _menhir_env _menhir_stack _menhir_s _v
        | SLASH ->
            _menhir_run71 _menhir_env (Obj.magic _menhir_stack) MenhirState119
        | STAR ->
            _menhir_run65 _menhir_env (Obj.magic _menhir_stack) MenhirState119
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState119)
    | MenhirState33 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | AND ->
            _menhir_run93 _menhir_env (Obj.magic _menhir_stack) MenhirState121
        | BOOL _v ->
            _menhir_run37 _menhir_env (Obj.magic _menhir_stack) MenhirState121 _v
        | CST _v ->
            _menhir_run36 _menhir_env (Obj.magic _menhir_stack) MenhirState121 _v
        | EQ ->
            _menhir_run91 _menhir_env (Obj.magic _menhir_stack) MenhirState121
        | GE ->
            _menhir_run89 _menhir_env (Obj.magic _menhir_stack) MenhirState121
        | GT ->
            _menhir_run87 _menhir_env (Obj.magic _menhir_stack) MenhirState121
        | IDENT _v ->
            _menhir_run35 _menhir_env (Obj.magic _menhir_stack) MenhirState121 _v
        | LE ->
            _menhir_run85 _menhir_env (Obj.magic _menhir_stack) MenhirState121
        | LPAR ->
            _menhir_run33 _menhir_env (Obj.magic _menhir_stack) MenhirState121
        | LSL ->
            _menhir_run69 _menhir_env (Obj.magic _menhir_stack) MenhirState121
        | LSR ->
            _menhir_run67 _menhir_env (Obj.magic _menhir_stack) MenhirState121
        | LT ->
            _menhir_run83 _menhir_env (Obj.magic _menhir_stack) MenhirState121
        | MINUS ->
            _menhir_run81 _menhir_env (Obj.magic _menhir_stack) MenhirState121
        | MOD ->
            _menhir_run75 _menhir_env (Obj.magic _menhir_stack) MenhirState121
        | NEQ ->
            _menhir_run79 _menhir_env (Obj.magic _menhir_stack) MenhirState121
        | OR ->
            _menhir_run77 _menhir_env (Obj.magic _menhir_stack) MenhirState121
        | PLUS ->
            _menhir_run73 _menhir_env (Obj.magic _menhir_stack) MenhirState121
        | RPAR ->
            _menhir_run117 _menhir_env (Obj.magic _menhir_stack) MenhirState121
        | SLASH ->
            _menhir_run71 _menhir_env (Obj.magic _menhir_stack) MenhirState121
        | STAR ->
            _menhir_run65 _menhir_env (Obj.magic _menhir_stack) MenhirState121
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState121)
    | MenhirState31 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | BOOL _v ->
            _menhir_run37 _menhir_env (Obj.magic _menhir_stack) MenhirState123 _v
        | CST _v ->
            _menhir_run36 _menhir_env (Obj.magic _menhir_stack) MenhirState123 _v
        | IDENT _v ->
            _menhir_run35 _menhir_env (Obj.magic _menhir_stack) MenhirState123 _v
        | LPAR ->
            _menhir_run33 _menhir_env (Obj.magic _menhir_stack) MenhirState123
        | AND | COMMA | ELSE | EOF | EQ | GE | GT | IN | LE | LSL | LSR | LT | MINUS | MOD | NEQ | OR | PLUS | RBRACE | RPAR | SEMI | SLASH | STAR | THEN ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let ((_menhir_stack, _menhir_s), _, (e : (Mml.expression))) = _menhir_stack in
            let _1 = () in
            let _v : (Mml.expression) = 
# 83 "mmlparser.mly"
                   ( Snd(e) )
# 2456 "mmlparser.ml"
             in
            _menhir_goto_expression _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState123)
    | MenhirState30 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | AND ->
            _menhir_run93 _menhir_env (Obj.magic _menhir_stack) MenhirState124
        | BOOL _v ->
            _menhir_run37 _menhir_env (Obj.magic _menhir_stack) MenhirState124 _v
        | CST _v ->
            _menhir_run36 _menhir_env (Obj.magic _menhir_stack) MenhirState124 _v
        | EOF ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_s = MenhirState124 in
            let _menhir_stack = Obj.magic _menhir_stack in
            let ((_menhir_stack, _menhir_s, (types : ((string * Types.namedtuple) list))), _, (code : (Mml.expression))) = _menhir_stack in
            let _3 = () in
            let _v : (
# 38 "mmlparser.mly"
      (Mml.program)
# 2483 "mmlparser.ml"
            ) = 
# 43 "mmlparser.mly"
                                           ( {types; code} )
# 2487 "mmlparser.ml"
             in
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_1 : (
# 38 "mmlparser.mly"
      (Mml.program)
# 2494 "mmlparser.ml"
            )) = _v in
            Obj.magic _1
        | EQ ->
            _menhir_run91 _menhir_env (Obj.magic _menhir_stack) MenhirState124
        | GE ->
            _menhir_run89 _menhir_env (Obj.magic _menhir_stack) MenhirState124
        | GT ->
            _menhir_run87 _menhir_env (Obj.magic _menhir_stack) MenhirState124
        | IDENT _v ->
            _menhir_run35 _menhir_env (Obj.magic _menhir_stack) MenhirState124 _v
        | LE ->
            _menhir_run85 _menhir_env (Obj.magic _menhir_stack) MenhirState124
        | LPAR ->
            _menhir_run33 _menhir_env (Obj.magic _menhir_stack) MenhirState124
        | LSL ->
            _menhir_run69 _menhir_env (Obj.magic _menhir_stack) MenhirState124
        | LSR ->
            _menhir_run67 _menhir_env (Obj.magic _menhir_stack) MenhirState124
        | LT ->
            _menhir_run83 _menhir_env (Obj.magic _menhir_stack) MenhirState124
        | MINUS ->
            _menhir_run81 _menhir_env (Obj.magic _menhir_stack) MenhirState124
        | MOD ->
            _menhir_run75 _menhir_env (Obj.magic _menhir_stack) MenhirState124
        | NEQ ->
            _menhir_run79 _menhir_env (Obj.magic _menhir_stack) MenhirState124
        | OR ->
            _menhir_run77 _menhir_env (Obj.magic _menhir_stack) MenhirState124
        | PLUS ->
            _menhir_run73 _menhir_env (Obj.magic _menhir_stack) MenhirState124
        | SLASH ->
            _menhir_run71 _menhir_env (Obj.magic _menhir_stack) MenhirState124
        | STAR ->
            _menhir_run65 _menhir_env (Obj.magic _menhir_stack) MenhirState124
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState124)
    | _ ->
        _menhir_fail ()

and _menhir_run39 : _menhir_env -> 'ttv_tail * _menhir_state * (Mml.expression) -> 'ttv_return =
  fun _menhir_env _menhir_stack ->
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | IDENT _v ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_env = _menhir_discard _menhir_env in
        let _menhir_stack = Obj.magic _menhir_stack in
        let (x : (
# 16 "mmlparser.mly"
       (string)
# 2548 "mmlparser.ml"
        )) = _v in
        let (_menhir_stack, _menhir_s, (e : (Mml.expression))) = _menhir_stack in
        let _2 = () in
        let _v : (Mml.expression) = 
# 73 "mmlparser.mly"
                                  ( StrGet(e, x) )
# 2555 "mmlparser.ml"
         in
        _menhir_goto_simple_expression _menhir_env _menhir_stack _menhir_s _v
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s

and _menhir_goto_typed_ident : _menhir_env -> 'ttv_tail -> _menhir_state -> (string * Types.ty) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    let _menhir_stack = (_menhir_stack, _menhir_s, _v) in
    match _menhir_s with
    | MenhirState5 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | RPAR ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_env = _menhir_discard _menhir_env in
            let _menhir_stack = Obj.magic _menhir_stack in
            let ((_menhir_stack, _menhir_s), _, (tid : (string * Types.ty))) = _menhir_stack in
            let _3 = () in
            let _1 = () in
            let _v : (string * Types.ty) = 
# 57 "mmlparser.mly"
                            ( tid )
# 2584 "mmlparser.ml"
             in
            _menhir_goto_typed_ident _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState22 | MenhirState4 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | SEMI ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_env = _menhir_discard _menhir_env in
            let _tok = _menhir_env._menhir_token in
            (match _tok with
            | IDENT _v ->
                _menhir_run6 _menhir_env (Obj.magic _menhir_stack) MenhirState22 _v
            | LPAR ->
                _menhir_run5 _menhir_env (Obj.magic _menhir_stack) MenhirState22
            | _ ->
                assert (not _menhir_env._menhir_error);
                _menhir_env._menhir_error <- true;
                _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState22)
        | RBRACE ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, (x : (string * Types.ty))) = _menhir_stack in
            let _v : (Types.namedtuple) = 
# 229 "/home/mohamedh/.opam/system/lib/menhir/standard.mly"
    ( [ x ] )
# 2617 "mmlparser.ml"
             in
            _menhir_goto_separated_nonempty_list_SEMI_typed_ident_ _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState110 | MenhirState45 | MenhirState44 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | IDENT _v ->
            _menhir_run6 _menhir_env (Obj.magic _menhir_stack) MenhirState45 _v
        | LPAR ->
            _menhir_run5 _menhir_env (Obj.magic _menhir_stack) MenhirState45
        | COLON | EQ ->
            _menhir_reduce31 _menhir_env (Obj.magic _menhir_stack) MenhirState45
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState45)
    | MenhirState56 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | RPAR ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_env = _menhir_discard _menhir_env in
            let _tok = _menhir_env._menhir_token in
            (match _tok with
            | ARROW ->
                let _menhir_stack = Obj.magic _menhir_stack in
                let _menhir_env = _menhir_discard _menhir_env in
                let _tok = _menhir_env._menhir_token in
                (match _tok with
                | BOOL _v ->
                    _menhir_run37 _menhir_env (Obj.magic _menhir_stack) MenhirState59 _v
                | CST _v ->
                    _menhir_run36 _menhir_env (Obj.magic _menhir_stack) MenhirState59 _v
                | FST ->
                    _menhir_run60 _menhir_env (Obj.magic _menhir_stack) MenhirState59
                | FUN ->
                    _menhir_run55 _menhir_env (Obj.magic _menhir_stack) MenhirState59
                | IDENT _v ->
                    _menhir_run35 _menhir_env (Obj.magic _menhir_stack) MenhirState59 _v
                | IF ->
                    _menhir_run54 _menhir_env (Obj.magic _menhir_stack) MenhirState59
                | LBRACE ->
                    _menhir_run51 _menhir_env (Obj.magic _menhir_stack) MenhirState59
                | LET ->
                    _menhir_run42 _menhir_env (Obj.magic _menhir_stack) MenhirState59
                | LPAR ->
                    _menhir_run41 _menhir_env (Obj.magic _menhir_stack) MenhirState59
                | MINUS ->
                    _menhir_run34 _menhir_env (Obj.magic _menhir_stack) MenhirState59
                | NOT ->
                    _menhir_run32 _menhir_env (Obj.magic _menhir_stack) MenhirState59
                | SND ->
                    _menhir_run31 _menhir_env (Obj.magic _menhir_stack) MenhirState59
                | _ ->
                    assert (not _menhir_env._menhir_error);
                    _menhir_env._menhir_error <- true;
                    _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState59)
            | _ ->
                assert (not _menhir_env._menhir_error);
                _menhir_env._menhir_error <- true;
                let _menhir_stack = Obj.magic _menhir_stack in
                let (_menhir_stack, _menhir_s, _) = _menhir_stack in
                _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | _ ->
        _menhir_fail ()

and _menhir_run13 : _menhir_env -> 'ttv_tail * _menhir_state * (Types.ty) -> 'ttv_return =
  fun _menhir_env _menhir_stack ->
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | IDENT _v ->
        _menhir_run11 _menhir_env (Obj.magic _menhir_stack) MenhirState13 _v
    | LPAR ->
        _menhir_run10 _menhir_env (Obj.magic _menhir_stack) MenhirState13
    | TBOOL ->
        _menhir_run9 _menhir_env (Obj.magic _menhir_stack) MenhirState13
    | TINT ->
        _menhir_run8 _menhir_env (Obj.magic _menhir_stack) MenhirState13
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState13

and _menhir_run16 : _menhir_env -> 'ttv_tail * _menhir_state * (Types.ty) -> 'ttv_return =
  fun _menhir_env _menhir_stack ->
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | IDENT _v ->
        _menhir_run11 _menhir_env (Obj.magic _menhir_stack) MenhirState16 _v
    | LPAR ->
        _menhir_run10 _menhir_env (Obj.magic _menhir_stack) MenhirState16
    | TBOOL ->
        _menhir_run9 _menhir_env (Obj.magic _menhir_stack) MenhirState16
    | TINT ->
        _menhir_run8 _menhir_env (Obj.magic _menhir_stack) MenhirState16
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState16

and _menhir_run33 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_stack = (_menhir_stack, _menhir_s) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | BOOL _v ->
        _menhir_run37 _menhir_env (Obj.magic _menhir_stack) MenhirState33 _v
    | CST _v ->
        _menhir_run36 _menhir_env (Obj.magic _menhir_stack) MenhirState33 _v
    | FST ->
        _menhir_run60 _menhir_env (Obj.magic _menhir_stack) MenhirState33
    | FUN ->
        _menhir_run55 _menhir_env (Obj.magic _menhir_stack) MenhirState33
    | IDENT _v ->
        _menhir_run35 _menhir_env (Obj.magic _menhir_stack) MenhirState33 _v
    | IF ->
        _menhir_run54 _menhir_env (Obj.magic _menhir_stack) MenhirState33
    | LBRACE ->
        _menhir_run51 _menhir_env (Obj.magic _menhir_stack) MenhirState33
    | LET ->
        _menhir_run42 _menhir_env (Obj.magic _menhir_stack) MenhirState33
    | LPAR ->
        _menhir_run41 _menhir_env (Obj.magic _menhir_stack) MenhirState33
    | MINUS ->
        _menhir_run34 _menhir_env (Obj.magic _menhir_stack) MenhirState33
    | NOT ->
        _menhir_run32 _menhir_env (Obj.magic _menhir_stack) MenhirState33
    | SND ->
        _menhir_run31 _menhir_env (Obj.magic _menhir_stack) MenhirState33
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState33

and _menhir_reduce31 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _v : ((string * Types.ty) list) = 
# 199 "/home/mohamedh/.opam/system/lib/menhir/standard.mly"
    ( [] )
# 2775 "mmlparser.ml"
     in
    _menhir_goto_list_typed_ident_ _menhir_env _menhir_stack _menhir_s _v

and _menhir_goto_loption_separated_nonempty_list_SEMI_field_def__ : _menhir_env -> 'ttv_tail -> _menhir_state -> ((string * Mml.expression) list) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    let _menhir_stack = (_menhir_stack, _menhir_s, _v) in
    let _menhir_stack = Obj.magic _menhir_stack in
    assert (not _menhir_env._menhir_error);
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | RBRACE ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_env = _menhir_discard _menhir_env in
        let _menhir_stack = Obj.magic _menhir_stack in
        let ((_menhir_stack, _menhir_s), _, (xs0 : ((string * Mml.expression) list))) = _menhir_stack in
        let _3 = () in
        let _1 = () in
        let _v : (Mml.expression) = let fields =
          let xs = xs0 in
          
# 220 "/home/mohamedh/.opam/system/lib/menhir/standard.mly"
    ( xs )
# 2798 "mmlparser.ml"
          
        in
        
# 84 "mmlparser.mly"
                                                       ( Struct(fields) )
# 2804 "mmlparser.ml"
         in
        _menhir_goto_expression _menhir_env _menhir_stack _menhir_s _v
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s

and _menhir_run52 : _menhir_env -> 'ttv_tail -> _menhir_state -> (
# 16 "mmlparser.mly"
       (string)
# 2817 "mmlparser.ml"
) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    let _menhir_stack = (_menhir_stack, _menhir_s, _v) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | EQ ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_env = _menhir_discard _menhir_env in
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | BOOL _v ->
            _menhir_run37 _menhir_env (Obj.magic _menhir_stack) MenhirState53 _v
        | CST _v ->
            _menhir_run36 _menhir_env (Obj.magic _menhir_stack) MenhirState53 _v
        | FST ->
            _menhir_run60 _menhir_env (Obj.magic _menhir_stack) MenhirState53
        | FUN ->
            _menhir_run55 _menhir_env (Obj.magic _menhir_stack) MenhirState53
        | IDENT _v ->
            _menhir_run35 _menhir_env (Obj.magic _menhir_stack) MenhirState53 _v
        | IF ->
            _menhir_run54 _menhir_env (Obj.magic _menhir_stack) MenhirState53
        | LBRACE ->
            _menhir_run51 _menhir_env (Obj.magic _menhir_stack) MenhirState53
        | LET ->
            _menhir_run42 _menhir_env (Obj.magic _menhir_stack) MenhirState53
        | LPAR ->
            _menhir_run41 _menhir_env (Obj.magic _menhir_stack) MenhirState53
        | MINUS ->
            _menhir_run34 _menhir_env (Obj.magic _menhir_stack) MenhirState53
        | NOT ->
            _menhir_run32 _menhir_env (Obj.magic _menhir_stack) MenhirState53
        | SND ->
            _menhir_run31 _menhir_env (Obj.magic _menhir_stack) MenhirState53
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState53)
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s

and _menhir_goto_simple_expression : _menhir_env -> 'ttv_tail -> _menhir_state -> (Mml.expression) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    let _menhir_stack = (_menhir_stack, _menhir_s, _v) in
    match _menhir_s with
    | MenhirState34 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | DOT ->
            _menhir_run39 _menhir_env (Obj.magic _menhir_stack)
        | AND | BOOL _ | COMMA | CST _ | ELSE | EOF | EQ | GE | GT | IDENT _ | IN | LE | LPAR | LSL | LSR | LT | MINUS | MOD | NEQ | OR | PLUS | RBRACE | RPAR | SEMI | SLASH | STAR | THEN ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let ((_menhir_stack, _menhir_s), _, (e : (Mml.expression))) = _menhir_stack in
            let _10 = () in
            let _v : (Mml.expression) = let op =
              let _1 = _10 in
              
# 96 "mmlparser.mly"
        ( Minus )
# 2884 "mmlparser.ml"
              
            in
            
# 79 "mmlparser.mly"
                              ( Unop(op, e) )
# 2890 "mmlparser.ml"
             in
            _menhir_goto_expression _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState30 | MenhirState31 | MenhirState33 | MenhirState118 | MenhirState41 | MenhirState114 | MenhirState112 | MenhirState108 | MenhirState50 | MenhirState53 | MenhirState98 | MenhirState96 | MenhirState54 | MenhirState93 | MenhirState91 | MenhirState89 | MenhirState87 | MenhirState85 | MenhirState83 | MenhirState81 | MenhirState79 | MenhirState77 | MenhirState75 | MenhirState73 | MenhirState71 | MenhirState69 | MenhirState67 | MenhirState65 | MenhirState59 | MenhirState60 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | DOT ->
            _menhir_run39 _menhir_env (Obj.magic _menhir_stack)
        | AND | BOOL _ | COMMA | CST _ | ELSE | EOF | EQ | GE | GT | IDENT _ | IN | LE | LPAR | LSL | LSR | LT | MINUS | MOD | NEQ | OR | PLUS | RBRACE | RPAR | SEMI | SLASH | STAR | THEN ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, (e : (Mml.expression))) = _menhir_stack in
            let _v : (Mml.expression) = 
# 78 "mmlparser.mly"
                      ( e )
# 2912 "mmlparser.ml"
             in
            _menhir_goto_expression _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState124 | MenhirState123 | MenhirState121 | MenhirState116 | MenhirState119 | MenhirState113 | MenhirState115 | MenhirState107 | MenhirState109 | MenhirState100 | MenhirState95 | MenhirState97 | MenhirState99 | MenhirState64 | MenhirState94 | MenhirState78 | MenhirState92 | MenhirState90 | MenhirState88 | MenhirState86 | MenhirState84 | MenhirState80 | MenhirState82 | MenhirState74 | MenhirState76 | MenhirState72 | MenhirState66 | MenhirState70 | MenhirState68 | MenhirState62 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | DOT ->
            _menhir_run39 _menhir_env (Obj.magic _menhir_stack)
        | AND | BOOL _ | COMMA | CST _ | ELSE | EOF | EQ | GE | GT | IDENT _ | IN | LE | LPAR | LSL | LSR | LT | MINUS | MOD | NEQ | OR | PLUS | RBRACE | RPAR | SEMI | SLASH | STAR | THEN ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let ((_menhir_stack, _menhir_s, (e1 : (Mml.expression))), _, (e2 : (Mml.expression))) = _menhir_stack in
            let _v : (Mml.expression) = 
# 85 "mmlparser.mly"
                                     ( App(e1, e2) )
# 2934 "mmlparser.ml"
             in
            _menhir_goto_expression _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState32 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | DOT ->
            _menhir_run39 _menhir_env (Obj.magic _menhir_stack)
        | AND | BOOL _ | COMMA | CST _ | ELSE | EOF | EQ | GE | GT | IDENT _ | IN | LE | LPAR | LSL | LSR | LT | MINUS | MOD | NEQ | OR | PLUS | RBRACE | RPAR | SEMI | SLASH | STAR | THEN ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let ((_menhir_stack, _menhir_s), _, (e : (Mml.expression))) = _menhir_stack in
            let _10 = () in
            let _v : (Mml.expression) = let op =
              let _1 = _10 in
              
# 97 "mmlparser.mly"
      ( Not )
# 2959 "mmlparser.ml"
              
            in
            
# 79 "mmlparser.mly"
                              ( Unop(op, e) )
# 2965 "mmlparser.ml"
             in
            _menhir_goto_expression _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | _ ->
        _menhir_fail ()

and _menhir_goto_ty : _menhir_env -> 'ttv_tail -> _menhir_state -> (Types.ty) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    let _menhir_stack = (_menhir_stack, _menhir_s, _v) in
    match _menhir_s with
    | MenhirState10 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | ARROW ->
            _menhir_run16 _menhir_env (Obj.magic _menhir_stack)
        | RPAR ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_env = _menhir_discard _menhir_env in
            let _menhir_stack = Obj.magic _menhir_stack in
            let ((_menhir_stack, _menhir_s), _, (alpha : (Types.ty))) = _menhir_stack in
            let _3 = () in
            let _1 = () in
            let _v : (Types.ty) = 
# 52 "mmlparser.mly"
                     ( alpha )
# 2998 "mmlparser.ml"
             in
            _menhir_goto_ty _menhir_env _menhir_stack _menhir_s _v
        | STAR ->
            _menhir_run13 _menhir_env (Obj.magic _menhir_stack)
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState13 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let ((_menhir_stack, _menhir_s, (alpha : (Types.ty))), _, (beta : (Types.ty))) = _menhir_stack in
        let _2 = () in
        let _v : (Types.ty) = 
# 50 "mmlparser.mly"
                        ( TPair(alpha, beta) )
# 3017 "mmlparser.ml"
         in
        _menhir_goto_ty _menhir_env _menhir_stack _menhir_s _v
    | MenhirState16 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | STAR ->
            _menhir_run13 _menhir_env (Obj.magic _menhir_stack)
        | COLON | EQ | IDENT _ | LPAR | RBRACE | RPAR | SEMI ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let ((_menhir_stack, _menhir_s, (alpha : (Types.ty))), _, (beta : (Types.ty))) = _menhir_stack in
            let _2 = () in
            let _v : (Types.ty) = 
# 49 "mmlparser.mly"
                         ( TFun(alpha, beta) )
# 3034 "mmlparser.ml"
             in
            _menhir_goto_ty _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState7 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | ARROW ->
            _menhir_run16 _menhir_env (Obj.magic _menhir_stack)
        | STAR ->
            _menhir_run13 _menhir_env (Obj.magic _menhir_stack)
        | COLON | EQ | IDENT _ | LPAR | RBRACE | RPAR | SEMI ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let ((_menhir_stack, _menhir_s, (x : (
# 16 "mmlparser.mly"
       (string)
# 3057 "mmlparser.ml"
            ))), _, (alpha : (Types.ty))) = _menhir_stack in
            let _2 = () in
            let _v : (string * Types.ty) = 
# 56 "mmlparser.mly"
                         ( (x, alpha) )
# 3063 "mmlparser.ml"
             in
            _menhir_goto_typed_ident _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState48 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | ARROW ->
            _menhir_run16 _menhir_env (Obj.magic _menhir_stack)
        | EQ ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_env = _menhir_discard _menhir_env in
            let _tok = _menhir_env._menhir_token in
            (match _tok with
            | BOOL _v ->
                _menhir_run37 _menhir_env (Obj.magic _menhir_stack) MenhirState50 _v
            | CST _v ->
                _menhir_run36 _menhir_env (Obj.magic _menhir_stack) MenhirState50 _v
            | FST ->
                _menhir_run60 _menhir_env (Obj.magic _menhir_stack) MenhirState50
            | FUN ->
                _menhir_run55 _menhir_env (Obj.magic _menhir_stack) MenhirState50
            | IDENT _v ->
                _menhir_run35 _menhir_env (Obj.magic _menhir_stack) MenhirState50 _v
            | IF ->
                _menhir_run54 _menhir_env (Obj.magic _menhir_stack) MenhirState50
            | LBRACE ->
                _menhir_run51 _menhir_env (Obj.magic _menhir_stack) MenhirState50
            | LET ->
                _menhir_run42 _menhir_env (Obj.magic _menhir_stack) MenhirState50
            | LPAR ->
                _menhir_run41 _menhir_env (Obj.magic _menhir_stack) MenhirState50
            | MINUS ->
                _menhir_run34 _menhir_env (Obj.magic _menhir_stack) MenhirState50
            | NOT ->
                _menhir_run32 _menhir_env (Obj.magic _menhir_stack) MenhirState50
            | SND ->
                _menhir_run31 _menhir_env (Obj.magic _menhir_stack) MenhirState50
            | _ ->
                assert (not _menhir_env._menhir_error);
                _menhir_env._menhir_error <- true;
                _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState50)
        | STAR ->
            _menhir_run13 _menhir_env (Obj.magic _menhir_stack)
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | _ ->
        _menhir_fail ()

and _menhir_fail : unit -> 'a =
  fun () ->
    Printf.fprintf Pervasives.stderr "Internal failure -- please contact the parser generator's developers.\n%!";
    assert false

and _menhir_run31 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_stack = (_menhir_stack, _menhir_s) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | BOOL _v ->
        _menhir_run37 _menhir_env (Obj.magic _menhir_stack) MenhirState31 _v
    | CST _v ->
        _menhir_run36 _menhir_env (Obj.magic _menhir_stack) MenhirState31 _v
    | FST ->
        _menhir_run60 _menhir_env (Obj.magic _menhir_stack) MenhirState31
    | FUN ->
        _menhir_run55 _menhir_env (Obj.magic _menhir_stack) MenhirState31
    | IDENT _v ->
        _menhir_run35 _menhir_env (Obj.magic _menhir_stack) MenhirState31 _v
    | IF ->
        _menhir_run54 _menhir_env (Obj.magic _menhir_stack) MenhirState31
    | LBRACE ->
        _menhir_run51 _menhir_env (Obj.magic _menhir_stack) MenhirState31
    | LET ->
        _menhir_run42 _menhir_env (Obj.magic _menhir_stack) MenhirState31
    | LPAR ->
        _menhir_run41 _menhir_env (Obj.magic _menhir_stack) MenhirState31
    | MINUS ->
        _menhir_run34 _menhir_env (Obj.magic _menhir_stack) MenhirState31
    | NOT ->
        _menhir_run32 _menhir_env (Obj.magic _menhir_stack) MenhirState31
    | SND ->
        _menhir_run31 _menhir_env (Obj.magic _menhir_stack) MenhirState31
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState31

and _menhir_run32 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_stack = (_menhir_stack, _menhir_s) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | BOOL _v ->
        _menhir_run37 _menhir_env (Obj.magic _menhir_stack) MenhirState32 _v
    | CST _v ->
        _menhir_run36 _menhir_env (Obj.magic _menhir_stack) MenhirState32 _v
    | IDENT _v ->
        _menhir_run35 _menhir_env (Obj.magic _menhir_stack) MenhirState32 _v
    | LPAR ->
        _menhir_run33 _menhir_env (Obj.magic _menhir_stack) MenhirState32
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState32

and _menhir_run34 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_stack = (_menhir_stack, _menhir_s) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | BOOL _v ->
        _menhir_run37 _menhir_env (Obj.magic _menhir_stack) MenhirState34 _v
    | CST _v ->
        _menhir_run36 _menhir_env (Obj.magic _menhir_stack) MenhirState34 _v
    | IDENT _v ->
        _menhir_run35 _menhir_env (Obj.magic _menhir_stack) MenhirState34 _v
    | LPAR ->
        _menhir_run33 _menhir_env (Obj.magic _menhir_stack) MenhirState34
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState34

and _menhir_run41 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_stack = (_menhir_stack, _menhir_s) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | BOOL _v ->
        _menhir_run37 _menhir_env (Obj.magic _menhir_stack) MenhirState41 _v
    | CST _v ->
        _menhir_run36 _menhir_env (Obj.magic _menhir_stack) MenhirState41 _v
    | FST ->
        _menhir_run60 _menhir_env (Obj.magic _menhir_stack) MenhirState41
    | FUN ->
        _menhir_run55 _menhir_env (Obj.magic _menhir_stack) MenhirState41
    | IDENT _v ->
        _menhir_run35 _menhir_env (Obj.magic _menhir_stack) MenhirState41 _v
    | IF ->
        _menhir_run54 _menhir_env (Obj.magic _menhir_stack) MenhirState41
    | LBRACE ->
        _menhir_run51 _menhir_env (Obj.magic _menhir_stack) MenhirState41
    | LET ->
        _menhir_run42 _menhir_env (Obj.magic _menhir_stack) MenhirState41
    | LPAR ->
        _menhir_run41 _menhir_env (Obj.magic _menhir_stack) MenhirState41
    | MINUS ->
        _menhir_run34 _menhir_env (Obj.magic _menhir_stack) MenhirState41
    | NOT ->
        _menhir_run32 _menhir_env (Obj.magic _menhir_stack) MenhirState41
    | SND ->
        _menhir_run31 _menhir_env (Obj.magic _menhir_stack) MenhirState41
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState41

and _menhir_run42 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_stack = (_menhir_stack, _menhir_s) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | IDENT _v ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = (_menhir_stack, _v) in
        let _menhir_env = _menhir_discard _menhir_env in
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | IDENT _v ->
            _menhir_run6 _menhir_env (Obj.magic _menhir_stack) MenhirState110 _v
        | LPAR ->
            _menhir_run5 _menhir_env (Obj.magic _menhir_stack) MenhirState110
        | EQ ->
            _menhir_reduce31 _menhir_env (Obj.magic _menhir_stack) MenhirState110
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState110)
    | REC ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_env = _menhir_discard _menhir_env in
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | IDENT _v ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_stack = (_menhir_stack, _v) in
            let _menhir_env = _menhir_discard _menhir_env in
            let _tok = _menhir_env._menhir_token in
            (match _tok with
            | IDENT _v ->
                _menhir_run6 _menhir_env (Obj.magic _menhir_stack) MenhirState44 _v
            | LPAR ->
                _menhir_run5 _menhir_env (Obj.magic _menhir_stack) MenhirState44
            | COLON ->
                _menhir_reduce31 _menhir_env (Obj.magic _menhir_stack) MenhirState44
            | _ ->
                assert (not _menhir_env._menhir_error);
                _menhir_env._menhir_error <- true;
                _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState44)
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s

and _menhir_run51 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_stack = (_menhir_stack, _menhir_s) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | IDENT _v ->
        _menhir_run52 _menhir_env (Obj.magic _menhir_stack) MenhirState51 _v
    | RBRACE ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_s = MenhirState51 in
        let _v : ((string * Mml.expression) list) = 
# 142 "/home/mohamedh/.opam/system/lib/menhir/standard.mly"
    ( [] )
# 3306 "mmlparser.ml"
         in
        _menhir_goto_loption_separated_nonempty_list_SEMI_field_def__ _menhir_env _menhir_stack _menhir_s _v
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState51

and _menhir_run54 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_stack = (_menhir_stack, _menhir_s) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | BOOL _v ->
        _menhir_run37 _menhir_env (Obj.magic _menhir_stack) MenhirState54 _v
    | CST _v ->
        _menhir_run36 _menhir_env (Obj.magic _menhir_stack) MenhirState54 _v
    | FST ->
        _menhir_run60 _menhir_env (Obj.magic _menhir_stack) MenhirState54
    | FUN ->
        _menhir_run55 _menhir_env (Obj.magic _menhir_stack) MenhirState54
    | IDENT _v ->
        _menhir_run35 _menhir_env (Obj.magic _menhir_stack) MenhirState54 _v
    | IF ->
        _menhir_run54 _menhir_env (Obj.magic _menhir_stack) MenhirState54
    | LBRACE ->
        _menhir_run51 _menhir_env (Obj.magic _menhir_stack) MenhirState54
    | LET ->
        _menhir_run42 _menhir_env (Obj.magic _menhir_stack) MenhirState54
    | LPAR ->
        _menhir_run41 _menhir_env (Obj.magic _menhir_stack) MenhirState54
    | MINUS ->
        _menhir_run34 _menhir_env (Obj.magic _menhir_stack) MenhirState54
    | NOT ->
        _menhir_run32 _menhir_env (Obj.magic _menhir_stack) MenhirState54
    | SND ->
        _menhir_run31 _menhir_env (Obj.magic _menhir_stack) MenhirState54
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState54

and _menhir_run35 : _menhir_env -> 'ttv_tail -> _menhir_state -> (
# 16 "mmlparser.mly"
       (string)
# 3352 "mmlparser.ml"
) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    let _menhir_env = _menhir_discard _menhir_env in
    let _menhir_stack = Obj.magic _menhir_stack in
    let (id : (
# 16 "mmlparser.mly"
       (string)
# 3360 "mmlparser.ml"
    )) = _v in
    let _v : (Mml.expression) = 
# 72 "mmlparser.mly"
           ( Var(id) )
# 3365 "mmlparser.ml"
     in
    _menhir_goto_simple_expression _menhir_env _menhir_stack _menhir_s _v

and _menhir_run55 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_stack = (_menhir_stack, _menhir_s) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | LPAR ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_env = _menhir_discard _menhir_env in
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | IDENT _v ->
            _menhir_run6 _menhir_env (Obj.magic _menhir_stack) MenhirState56 _v
        | LPAR ->
            _menhir_run5 _menhir_env (Obj.magic _menhir_stack) MenhirState56
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState56)
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s

and _menhir_run60 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_stack = (_menhir_stack, _menhir_s) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | BOOL _v ->
        _menhir_run37 _menhir_env (Obj.magic _menhir_stack) MenhirState60 _v
    | CST _v ->
        _menhir_run36 _menhir_env (Obj.magic _menhir_stack) MenhirState60 _v
    | FST ->
        _menhir_run60 _menhir_env (Obj.magic _menhir_stack) MenhirState60
    | FUN ->
        _menhir_run55 _menhir_env (Obj.magic _menhir_stack) MenhirState60
    | IDENT _v ->
        _menhir_run35 _menhir_env (Obj.magic _menhir_stack) MenhirState60 _v
    | IF ->
        _menhir_run54 _menhir_env (Obj.magic _menhir_stack) MenhirState60
    | LBRACE ->
        _menhir_run51 _menhir_env (Obj.magic _menhir_stack) MenhirState60
    | LET ->
        _menhir_run42 _menhir_env (Obj.magic _menhir_stack) MenhirState60
    | LPAR ->
        _menhir_run41 _menhir_env (Obj.magic _menhir_stack) MenhirState60
    | MINUS ->
        _menhir_run34 _menhir_env (Obj.magic _menhir_stack) MenhirState60
    | NOT ->
        _menhir_run32 _menhir_env (Obj.magic _menhir_stack) MenhirState60
    | SND ->
        _menhir_run31 _menhir_env (Obj.magic _menhir_stack) MenhirState60
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState60

and _menhir_run36 : _menhir_env -> 'ttv_tail -> _menhir_state -> (
# 14 "mmlparser.mly"
       (int)
# 3433 "mmlparser.ml"
) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    let _menhir_env = _menhir_discard _menhir_env in
    let _menhir_stack = Obj.magic _menhir_stack in
    let (n : (
# 14 "mmlparser.mly"
       (int)
# 3441 "mmlparser.ml"
    )) = _v in
    let _v : (Mml.expression) = 
# 70 "mmlparser.mly"
        ( Cst(n) )
# 3446 "mmlparser.ml"
     in
    _menhir_goto_simple_expression _menhir_env _menhir_stack _menhir_s _v

and _menhir_run37 : _menhir_env -> 'ttv_tail -> _menhir_state -> (
# 15 "mmlparser.mly"
       (bool)
# 3453 "mmlparser.ml"
) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    let _menhir_env = _menhir_discard _menhir_env in
    let _menhir_stack = Obj.magic _menhir_stack in
    let (b : (
# 15 "mmlparser.mly"
       (bool)
# 3461 "mmlparser.ml"
    )) = _v in
    let _v : (Mml.expression) = 
# 71 "mmlparser.mly"
         ( Bool(b) )
# 3466 "mmlparser.ml"
     in
    _menhir_goto_simple_expression _menhir_env _menhir_stack _menhir_s _v

and _menhir_run8 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_env = _menhir_discard _menhir_env in
    let _menhir_stack = Obj.magic _menhir_stack in
    let _1 = () in
    let _v : (Types.ty) = 
# 47 "mmlparser.mly"
       ( TInt )
# 3478 "mmlparser.ml"
     in
    _menhir_goto_ty _menhir_env _menhir_stack _menhir_s _v

and _menhir_run9 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_env = _menhir_discard _menhir_env in
    let _menhir_stack = Obj.magic _menhir_stack in
    let _1 = () in
    let _v : (Types.ty) = 
# 48 "mmlparser.mly"
        ( TBool )
# 3490 "mmlparser.ml"
     in
    _menhir_goto_ty _menhir_env _menhir_stack _menhir_s _v

and _menhir_run10 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_stack = (_menhir_stack, _menhir_s) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | IDENT _v ->
        _menhir_run11 _menhir_env (Obj.magic _menhir_stack) MenhirState10 _v
    | LPAR ->
        _menhir_run10 _menhir_env (Obj.magic _menhir_stack) MenhirState10
    | TBOOL ->
        _menhir_run9 _menhir_env (Obj.magic _menhir_stack) MenhirState10
    | TINT ->
        _menhir_run8 _menhir_env (Obj.magic _menhir_stack) MenhirState10
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState10

and _menhir_run11 : _menhir_env -> 'ttv_tail -> _menhir_state -> (
# 16 "mmlparser.mly"
       (string)
# 3516 "mmlparser.ml"
) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    let _menhir_env = _menhir_discard _menhir_env in
    let _menhir_stack = Obj.magic _menhir_stack in
    let (id : (
# 16 "mmlparser.mly"
       (string)
# 3524 "mmlparser.ml"
    )) = _v in
    let _v : (Types.ty) = 
# 51 "mmlparser.mly"
           ( TNamed(id) )
# 3529 "mmlparser.ml"
     in
    _menhir_goto_ty _menhir_env _menhir_stack _menhir_s _v

and _menhir_goto_list_type_def_ : _menhir_env -> 'ttv_tail -> _menhir_state -> ((string * Types.namedtuple) list) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    let _menhir_stack = (_menhir_stack, _menhir_s, _v) in
    match _menhir_s with
    | MenhirState27 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let ((_menhir_stack, _menhir_s, (x : (string * Types.namedtuple))), _, (xs : ((string * Types.namedtuple) list))) = _menhir_stack in
        let _v : ((string * Types.namedtuple) list) = 
# 201 "/home/mohamedh/.opam/system/lib/menhir/standard.mly"
    ( x :: xs )
# 3544 "mmlparser.ml"
         in
        _menhir_goto_list_type_def_ _menhir_env _menhir_stack _menhir_s _v
    | MenhirState0 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | BOOL _v ->
            _menhir_run37 _menhir_env (Obj.magic _menhir_stack) MenhirState30 _v
        | CST _v ->
            _menhir_run36 _menhir_env (Obj.magic _menhir_stack) MenhirState30 _v
        | FST ->
            _menhir_run60 _menhir_env (Obj.magic _menhir_stack) MenhirState30
        | FUN ->
            _menhir_run55 _menhir_env (Obj.magic _menhir_stack) MenhirState30
        | IDENT _v ->
            _menhir_run35 _menhir_env (Obj.magic _menhir_stack) MenhirState30 _v
        | IF ->
            _menhir_run54 _menhir_env (Obj.magic _menhir_stack) MenhirState30
        | LBRACE ->
            _menhir_run51 _menhir_env (Obj.magic _menhir_stack) MenhirState30
        | LET ->
            _menhir_run42 _menhir_env (Obj.magic _menhir_stack) MenhirState30
        | LPAR ->
            _menhir_run41 _menhir_env (Obj.magic _menhir_stack) MenhirState30
        | MINUS ->
            _menhir_run34 _menhir_env (Obj.magic _menhir_stack) MenhirState30
        | NOT ->
            _menhir_run32 _menhir_env (Obj.magic _menhir_stack) MenhirState30
        | SND ->
            _menhir_run31 _menhir_env (Obj.magic _menhir_stack) MenhirState30
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState30)
    | _ ->
        _menhir_fail ()

and _menhir_goto_loption_separated_nonempty_list_SEMI_typed_ident__ : _menhir_env -> 'ttv_tail -> _menhir_state -> (Types.namedtuple) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    let _menhir_stack = (_menhir_stack, _menhir_s, _v) in
    let _menhir_stack = Obj.magic _menhir_stack in
    assert (not _menhir_env._menhir_error);
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | RBRACE ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_env = _menhir_discard _menhir_env in
        let _menhir_stack = Obj.magic _menhir_stack in
        let (((_menhir_stack, _menhir_s), (id : (
# 16 "mmlparser.mly"
       (string)
# 3597 "mmlparser.ml"
        ))), _, (xs0 : (Types.namedtuple))) = _menhir_stack in
        let _6 = () in
        let _4 = () in
        let _3 = () in
        let _1 = () in
        let _v : (string * Types.namedtuple) = let fields =
          let xs = xs0 in
          
# 220 "/home/mohamedh/.opam/system/lib/menhir/standard.mly"
    ( xs )
# 3608 "mmlparser.ml"
          
        in
        
# 62 "mmlparser.mly"
    ( (id, fields) )
# 3614 "mmlparser.ml"
         in
        let _menhir_stack = (_menhir_stack, _menhir_s, _v) in
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | TYPE ->
            _menhir_run1 _menhir_env (Obj.magic _menhir_stack) MenhirState27
        | BOOL _ | CST _ | FST | FUN | IDENT _ | IF | LBRACE | LET | LPAR | MINUS | NOT | SND ->
            _menhir_reduce29 _menhir_env (Obj.magic _menhir_stack) MenhirState27
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState27)
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s

and _menhir_run5 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_stack = (_menhir_stack, _menhir_s) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | IDENT _v ->
        _menhir_run6 _menhir_env (Obj.magic _menhir_stack) MenhirState5 _v
    | LPAR ->
        _menhir_run5 _menhir_env (Obj.magic _menhir_stack) MenhirState5
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState5

and _menhir_run6 : _menhir_env -> 'ttv_tail -> _menhir_state -> (
# 16 "mmlparser.mly"
       (string)
# 3654 "mmlparser.ml"
) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    let _menhir_stack = (_menhir_stack, _menhir_s, _v) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | COLON ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_env = _menhir_discard _menhir_env in
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | IDENT _v ->
            _menhir_run11 _menhir_env (Obj.magic _menhir_stack) MenhirState7 _v
        | LPAR ->
            _menhir_run10 _menhir_env (Obj.magic _menhir_stack) MenhirState7
        | TBOOL ->
            _menhir_run9 _menhir_env (Obj.magic _menhir_stack) MenhirState7
        | TINT ->
            _menhir_run8 _menhir_env (Obj.magic _menhir_stack) MenhirState7
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState7)
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s

and _menhir_errorcase : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    match _menhir_s with
    | MenhirState124 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState123 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState121 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState119 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState118 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState116 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState115 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState114 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState113 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState112 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState110 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let ((_menhir_stack, _menhir_s), _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState109 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState108 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState107 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState105 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState100 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState99 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState98 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState97 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState96 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState95 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState94 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState93 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState92 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState91 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState90 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState89 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState88 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState87 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState86 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState85 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState84 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState83 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState82 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState81 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState80 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState79 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState78 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState77 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState76 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState75 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState74 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState73 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState72 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState71 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState70 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState69 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState68 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState67 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState66 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState65 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState64 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState62 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState60 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState59 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState56 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState54 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState53 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState51 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState50 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState48 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState45 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState44 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let ((_menhir_stack, _menhir_s), _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState41 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState34 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState33 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState32 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState31 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState30 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState27 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState22 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState16 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState13 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState10 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState7 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState5 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState4 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let ((_menhir_stack, _menhir_s), _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState0 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        raise _eRR

and _menhir_reduce29 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _v : ((string * Types.namedtuple) list) = 
# 199 "/home/mohamedh/.opam/system/lib/menhir/standard.mly"
    ( [] )
# 4005 "mmlparser.ml"
     in
    _menhir_goto_list_type_def_ _menhir_env _menhir_stack _menhir_s _v

and _menhir_run1 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_stack = (_menhir_stack, _menhir_s) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | IDENT _v ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = (_menhir_stack, _v) in
        let _menhir_env = _menhir_discard _menhir_env in
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | EQ ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_env = _menhir_discard _menhir_env in
            let _tok = _menhir_env._menhir_token in
            (match _tok with
            | LBRACE ->
                let _menhir_stack = Obj.magic _menhir_stack in
                let _menhir_env = _menhir_discard _menhir_env in
                let _tok = _menhir_env._menhir_token in
                (match _tok with
                | IDENT _v ->
                    _menhir_run6 _menhir_env (Obj.magic _menhir_stack) MenhirState4 _v
                | LPAR ->
                    _menhir_run5 _menhir_env (Obj.magic _menhir_stack) MenhirState4
                | RBRACE ->
                    let _menhir_stack = Obj.magic _menhir_stack in
                    let _menhir_s = MenhirState4 in
                    let _v : (Types.namedtuple) = 
# 142 "/home/mohamedh/.opam/system/lib/menhir/standard.mly"
    ( [] )
# 4041 "mmlparser.ml"
                     in
                    _menhir_goto_loption_separated_nonempty_list_SEMI_typed_ident__ _menhir_env _menhir_stack _menhir_s _v
                | _ ->
                    assert (not _menhir_env._menhir_error);
                    _menhir_env._menhir_error <- true;
                    _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState4)
            | _ ->
                assert (not _menhir_env._menhir_error);
                _menhir_env._menhir_error <- true;
                let _menhir_stack = Obj.magic _menhir_stack in
                let ((_menhir_stack, _menhir_s), _) = _menhir_stack in
                _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let ((_menhir_stack, _menhir_s), _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s

and _menhir_discard : _menhir_env -> _menhir_env =
  fun _menhir_env ->
    let lexer = _menhir_env._menhir_lexer in
    let lexbuf = _menhir_env._menhir_lexbuf in
    let _tok = lexer lexbuf in
    {
      _menhir_lexer = lexer;
      _menhir_lexbuf = lexbuf;
      _menhir_token = _tok;
      _menhir_error = false;
    }

and program : (Lexing.lexbuf -> token) -> Lexing.lexbuf -> (
# 38 "mmlparser.mly"
      (Mml.program)
# 4082 "mmlparser.ml"
) =
  fun lexer lexbuf ->
    let _menhir_env = let _tok = Obj.magic () in
    {
      _menhir_lexer = lexer;
      _menhir_lexbuf = lexbuf;
      _menhir_token = _tok;
      _menhir_error = false;
    } in
    Obj.magic (let _menhir_stack = ((), _menhir_env._menhir_lexbuf.Lexing.lex_curr_p) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | TYPE ->
        _menhir_run1 _menhir_env (Obj.magic _menhir_stack) MenhirState0
    | BOOL _ | CST _ | FST | FUN | IDENT _ | IF | LBRACE | LET | LPAR | MINUS | NOT | SND ->
        _menhir_reduce29 _menhir_env (Obj.magic _menhir_stack) MenhirState0
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState0)

# 233 "/home/mohamedh/.opam/system/lib/menhir/standard.mly"
  

# 4108 "mmlparser.ml"
