open Types

module Tpl_map = Map.Make(String)

let string_index = Tpl_map.empty


 



let translate_program prog =

  (* faut ajouter S un dict ou map avec les indices adapté pour les reutiliser avec le get *)
  let split_explist el name nb= match el with
    | [] ->  []     
    | [x] ->  (match x with 
                | (s,ex) ->   str_index = Tpl_map.add s nb 
                 [ tr_expr ex]   )

    | x::l -> (match x with
                | (s,ex) ->  let ret = [tr_expr ex]  in
                  str_index = Tpl_map.add s nb 
                  let l2 = split_explist l name (nb + 1) in

                  [ret;l2] 
                  )

  let rec tr_expr te = match te.Tml.e with
    | Tml.Cst n -> Fun.Cst n
    | Tml.Bool b -> Fun.Bool b      
    | Tml.Var x -> Fun.Var x
    (* Attention dans Binop : cas particulier à faire pour Eq *)
    | Tml.Binop(op, e1, e2) -> Fun.Binop(op, tr_expr e1, tr_expr e2)
    | Tml.Unop(op, e1) -> Fun.Unop(op, tr_expr e1)
    | Tml.LetIn(x, e1, e2) -> Fun.LetIn(x, tr_expr e1, tr_expr e2)
    | Tml.If(e0, e1, e2) -> Fun.If(tr_expr e0, tr_expr e1, tr_expr e2) (*tested *)
    (*not yet tested *)
    | Tml.App(e0, e1)-> Fun.App(tr_expr e0, tr_expr e1)
    | Tml.Fun(s, t, e)->  Fun.Fun(s, tr_expr e) 
 
    | Tml.LetRec(x,t, e1, e2) -> Fun.LetRec(x, tr_expr e1, tr_expr e2)
    | Tml.Pair(e1, e2)->  Fun.Tpl( [ (tr_expr e1); (tr_expr e2) ] ) (*a verifier si c la bonne sntx pr une liste *)
    (* !!! check si 1 et 2 ou 0 et 1!!!!- à relire dans le sujet ou demnder au prof? *)
    | Tml.Fst(e1)-> Fun.TplGet( tr_expr e1, 0)  
    | Tml.Snd(e2)-> Fun.TplGet( tr_expr e2, 1) 
    (* structures *)
    
    (* | Struct(expList) ->  *)

    (* exp: l'expression et s le nom du champ*)
    | StrGet(exp, s) ->  
      let indice = Tpl_map.find s string_index in
      Fun.TplGet( tr_expr exp, indice )
    

  in

  tr_expr prog.Tml.code
