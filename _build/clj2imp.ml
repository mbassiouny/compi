module STbl = Map.Make(String)

let rec count_elements l =
  match l with
  | [] ->  0
  | _::t -> 1 + count_elements t 

  (* let rec fill_array arr elist iseq iexp count = 
    (match elist with 
            | [x] ->  let seq, exp = tr_expr x env 
                        in
                        iseq:=  Imp.array_set(arr count exp )
                        iexp:= exp
            | x::l -> let seq, exp = tr_expr x env 
                      in
                      iseq:=  Imp.array_set(arr count exp )
                      iexp:= exp
                      fill_array arr l iseq iexp (count+1)
    ) *)

let tr_var v env = match v with
  | Clj.Name(x) ->
    Imp.(if STbl.mem x env then Var(STbl.find x env) else Var x)
      
  | Clj.CVar(n) ->
    Imp.(array_get (Var "closure") (Cst n))
      
let tr_expr e env =
  let cpt = ref (-1) in
  let vars = ref [] in
  let new_var id =
    incr cpt;
    let v = Printf.sprintf "%s_%i" id !cpt in
    vars := v :: !vars;
    v
  in
  


  let rec tr_expr (e: Clj.expression) (env: string STbl.t):
      Imp.sequence * Imp.expression =


  
    match e with
      | Clj.Cst(n) ->
        [], Imp.Cst(n)

      | Clj.Bool(b) ->
        [], Imp.Bool(b)

      | Clj.Var(v) ->
        [], tr_var v env

      | Clj.Unop(op, e1) ->
        let is1, te1 = tr_expr e1 env in
        is1 , Imp.Unop(op, te1)

      | Clj.Binop(op, e1, e2) ->
        let is1, te1 = tr_expr e1 env in
        let is2, te2 = tr_expr e2 env in
        is1 @ is2, Imp.Binop(op, te1, te2)
          
      | Clj.LetIn(x, e1, e2) ->
        let lv = new_var x in
        let is1, t1 = tr_expr e1 env in
        let is2, t2 = tr_expr e2 (STbl.add x lv env) in
        Imp.(is1 @ [Set(lv, t1)] @ is2, t2)

        (* | Clj.FunRef(s) -> *)
        (*    
      | Clj.Tpl(elist) -> (match elist with 
          | []-> failwith "empty"
          | [x]-> failwith "1 elem"
          | x::l-> failwith (type x) ) *)

      (* comme on traduit une decalaration donc elle est tjrs dans un let  *)
      (* |Clj.LetIn (s, Clj.Tpl(elist), e) -> 
        let taille = count_elements(elist)  in
        let len = Imp.Cst(taille) in
        let is1, t1 = tr_expr e env in
        let arr = Imp.array_create(len ) in  (*  arr create retourne une expression, donc arr une expression imp*)
        let iseq =ref  []
        iexp := []
        (* cette ligne doit etre presente mais problem de syntaxe a verfier... *)
        (* fill_array( arr elist iseq iexp 0) *) 
        iseq, iexp
         *)
      
      (* | Clj.TplGet(e1,i) -> 
        let is1, t1 = tr_expr e1 env in
        Imp.array_get(t1, i) *)
         
       
      (* | Clj.App(e1,e2) ->
        let fct_ptr = Clj.(TplGet(e1,0)) in 
        let fct = Imp.Deref(tr_expr fct_ptr env) in
        let is1, t1 = tr_expr e1 env in
        let is2, t2 = tr_expr e2 env in        
        is1 @ is2, Imp.PCall( fct ,[t2] )
(*      | Clj.FunRef(s) -> failwith "not yet implemented"
 *) *)
      | Clj.LetRec(x, e1, e2) -> 
      (* to be checked *)
        let lv = new_var x in 
        let is1, t1 = tr_expr e1 env in
        let is2, t2 = tr_expr e2 (STbl.add x lv env) in
        
        Imp.(is1 @ [Set(lv, t1)] @ is2, t2)

      | Clj.If(e1, e2, e3) ->
        let is1, t1 = tr_expr e1 env in
        let is2, t2 = tr_expr e2 env in
        let is3, t3 = tr_expr e3 env in
        (* is2 @ is3, Imp.If(t1,is2,is3a ) *)
        Imp.(is2 @ [If(t1,is2,is3 ) ] @ is3, t3)
  in
    
  let is, te = tr_expr e env in
  is, te, !vars

    
let tr_fdef fdef =
  let env =
    let x = Clj.(fdef.param) in
    STbl.add x ("param_" ^ x) STbl.empty
  in
  let is, te, locals = tr_expr Clj.(fdef.code) env in
  Imp.({
    name = Clj.(fdef.name);
    code = is @ [Return te];
    params = ["param_" ^ Clj.(fdef.param); "closure"];
    locals = locals;
  })


let translate_program prog =
  let functions = List.map tr_fdef Clj.(prog.functions) in
  let is, te, globals = tr_expr Clj.(prog.code) STbl.empty in
  let main = Imp.(is @ [Expr(Call("print_int", [te]))]) in
  Imp.({main; functions; globals})
    
