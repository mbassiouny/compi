open Types

type expression = { e: expr; t: ty }
and expr =    
  | Cst   of int
  | Bool  of bool
  | Var   of string
  | Unop  of Ops.unop * expression
  | Binop of Ops.binop * expression * expression
  | Pair  of expression * expression
  | Fst   of expression
  | Snd   of expression
  | Struct of (string * expression) list
  | StrGet of expression * string
  | Fun   of string * ty * expression
  | App   of expression * expression
  | If    of expression * expression * expression
  | LetIn of string * expression * expression
  | LetRec of string * ty * expression * expression

type program = {
  types: (string * namedtuple) list;
  code: expression;
}
