(* *MML* / FUN / IMP *)

let usage = "usage: ./mmlc file.mml"

let spec = []
  
let file =
    let file = ref None in
    let set_file s =
      if not (Filename.check_suffix s ".mml") then
        raise (Arg.Bad "no .mml extension");
      file := Some s
    in
    Arg.parse spec set_file usage;
    match !file with Some f -> f | None -> Arg.usage spec usage; exit 1

let () =
  let c  = open_in file in
  let lb = Lexing.from_channel c in
  let prog = Mmlparser.program Mmllexer.token lb
  in
  close_in c;
  try
    let progtml = Mml2tml.type_program prog in
    let progfun = Tml2fun.translate_program progtml in
    let progclj = Fun2clj.translate_program progfun in
    let progimp = Clj2imp.translate_program progclj in
    let output_file = (Filename.chop_suffix file ".fun") ^ ".imp" in
    let out = open_out output_file in
    Imppp.pp_program progimp out;
    (* Fun.program progfun; *)
    close_out out;
    exit 0
  with
    | Types.TypeError(actual, expected) ->
      Printf.printf
        "Type error. Expected %s, got %s\n"
        (Types.ty_to_string expected) (Types.ty_to_string actual);
      exit 1
    | Types.UnexpectedType(actual, expected) ->
      Printf.printf
        "Type error. Expected %s, got %s\n"
        expected (Types.ty_to_string actual);
      exit 1
    | Types.UnknownStruct ->
      Printf.printf "Unrecognized field list\n";
      exit 1

    
