
(* The type of tokens. *)

type token = 
  | TYPE
  | TINT
  | THEN
  | TBOOL
  | STAR
  | SND
  | SLASH
  | SEMI
  | RPAR
  | REC
  | RBRACE
  | PLUS
  | OR
  | NOT
  | NEQ
  | MOD
  | MINUS
  | LT
  | LSR
  | LSL
  | LPAR
  | LET
  | LE
  | LBRACE
  | IN
  | IF
  | IDENT of (string)
  | GT
  | GE
  | FUN
  | FST
  | EQ
  | EOF
  | ELSE
  | DOT
  | CST of (int)
  | COMMA
  | COLON
  | BOOL of (bool)
  | ARROW
  | AND

(* This exception is raised by the monolithic API functions. *)

exception Error

(* The monolithic API. *)

val program: (Lexing.lexbuf -> token) -> Lexing.lexbuf -> (Mml.program)
